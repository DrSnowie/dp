
#include <vst_gpu_sortsites.h>

namespace VST {
namespace GPU {

const GLchar placeholderSortSitesSrc[] = { R".(
layout(local_size_x=32)in;

void main() {
	uint x = gl_GlobalInvocationID.x;
//	uint y = gl_GlobalInvocationID.y;
	
	int siteCount = info[siteCountIndex];
	
	
	if (int(x) < siteCount) {
		int startingIndex = siteCount * int(x);
		int endingIndex = startingIndex + siteCount - 1;
		const vec3 mySite = sites[x].xyz;
		
		sortedSites[x].x = startingIndex;
		for (int i = 0; i < siteCount; ++i) {
			const vec3 site = sites[i].xyz;
			const vec3 distanceVector = site - mySite;
			sortedSites[startingIndex+i] = vec4(site, dot(distanceVector, distanceVector));
		}
		
		for (int j = 0; j < siteCount; ++j) {
			for (int i = startingIndex; i < endingIndex; ++i) {
				if (sortedSites[i].w > sortedSites[i+1].w) {
					const vec4 site = sortedSites[i+1];
					sortedSites[i+1] = sortedSites[i];
					sortedSites[i] = site;
				}
			}
		}
	}
	
}

)." };

const GLchar radixSortSitesSrc[] = { R".(
layout(local_size_x=32)in;

shared int classOffsets[32*2]; 

void main() {
	const uint workGroupId = gl_WorkGroupID.x;
	const uint workGroupSize = gl_WorkGroupSize.x;
	const uint workGroupCount = gl_NumWorkGroups.x;
	const uint x = gl_LocalInvocationID.x;
	
	int siteCount = info[siteCountIndex];
	
	
	const int partSize = siteCount < int(workGroupSize) ? 1 : (siteCount + int(workGroupSize) - 1) / int(workGroupSize) ;
	const int indexOffset = partSize * int(x);
	const int maxIndex = indexOffset + partSize > siteCount ? siteCount : indexOffset + partSize;
	
	
	for (int siteIndex = int(workGroupId); siteIndex < siteCount; siteIndex += int(workGroupCount)) {
		
		
		const int sortedSitesOffset = siteIndex * siteCount;
		
		
		const vec4 mySite = sites[siteIndex];
		for (int i = indexOffset; i < maxIndex; ++i) {
			const vec3 site = sites[i].xyz;
			const vec3 distanceVector = site - mySite.xyz;
			sortingAuxBuffer[sortedSitesOffset+i] = vec4(site, dot(distanceVector, distanceVector));
		}
		
		
		
		
		for (int radixIteration = 0; radixIteration < 31; ++radixIteration) {
			classOffsets[x] = 0;
			classOffsets[x+32] = 0;
			int class0Offset = 0;
			int class1Offset = 0;
			int class0Size = 0;
			
			
			if (radixIteration % 2 == 0) {
				for (int itemIndex = indexOffset; itemIndex < maxIndex; ++itemIndex) {
					const int radixValue = (floatBitsToInt(sortingAuxBuffer[sortedSitesOffset+itemIndex].w) >> radixIteration) & 1;
					if (radixValue == 0) {
						sortingAuxBuffer[sortedSitesOffset+itemIndex] = -sortingAuxBuffer[sortedSitesOffset+itemIndex];
						classOffsets[x]++;
					} else if (radixValue == 1) {
						classOffsets[x+32]++;
					}
				}
			} else {
				for (int itemIndex = indexOffset; itemIndex < maxIndex; ++itemIndex) {
					const int radixValue = (floatBitsToInt(sortedSites[sortedSitesOffset+itemIndex].w) >> radixIteration) & 1;
					if (radixValue == 0) {
						sortedSites[sortedSitesOffset+itemIndex] = -sortedSites[sortedSitesOffset+itemIndex];
						classOffsets[x]++;
					} else if (radixValue == 1) {
						classOffsets[x+32]++;
					}
				}
			}
			
			
			
			//TODO the parallel prefix sum could be used here but for 32 elements, what is the point - do this in spare time maybe
			for (int i = 0; i < 32; ++i) {
				if (i < x) {
					class0Offset += classOffsets[i];
					class1Offset += classOffsets[i+32];
				}
				class0Size += classOffsets[i];
			}
			
			const uint signBit = 2147483648;
			if (radixIteration % 2 == 0) {
				for (int itemIndex = indexOffset; itemIndex < maxIndex; ++itemIndex) {
					if ((floatBitsToUint(sortingAuxBuffer[sortedSitesOffset+itemIndex].w) & signBit) == signBit) sortedSites[sortedSitesOffset+class0Offset++] = -sortingAuxBuffer[sortedSitesOffset+itemIndex];
					else sortedSites[sortedSitesOffset+class0Size+class1Offset++] = sortingAuxBuffer[sortedSitesOffset+itemIndex];
				}
			} else {
				for (int itemIndex = indexOffset; itemIndex < maxIndex; ++itemIndex) {
					if ((floatBitsToUint(sortedSites[sortedSitesOffset+itemIndex].w) & signBit) == signBit) sortingAuxBuffer[sortedSitesOffset+class0Offset++] = -sortedSites[sortedSitesOffset+itemIndex];
					else sortingAuxBuffer[sortedSitesOffset+class0Size+class1Offset++] = sortedSites[sortedSitesOffset+itemIndex];
				}
			}
		}
	}
}

)." };

}} //end namespace VST::GPU

