//autor: Michael Ončo
#pragma once


#include <vector>
#include <glm/glm.hpp>

namespace VST {
namespace CPU {

struct CellVerticesInfo {
	
	const glm::vec4 *usedSite;
	const std::vector<glm::vec4> *usedVertices;
	
	std::vector<float> squareDistances;
	
	void init(const std::vector<glm::vec4> &vertices, const glm::vec4 &site) {
		const int size = vertices.size();
		
		squareDistances.clear();
		
		usedSite = &site;
		usedVertices = &vertices;
		
		squareDistances.reserve(size);
		
		for (int i = 0; i < size; ++i) {
			const glm::vec3 distanceVector = site - vertices[i];
			const float distance = glm::dot(distanceVector, distanceVector);
			squareDistances.push_back(distance);
		}
	}
	
	void add(const int index) {
		assert(usedVertices->size() > index);
		
		const glm::vec4 &vertex = (*usedVertices)[index];
		
		const glm::vec4 distanceVector = vertex - *usedSite;
		const float distance = glm::dot(distanceVector, distanceVector);
		
		if (squareDistances.size() > index) {
			squareDistances[index] = distance;
		}
		else {
			squareDistances.push_back(distance);
		}
	}
	
	void remove(const int index) {
		assert(squareDistances.size() > index);
		squareDistances[index] = -1.0f;
	}
	
	float simpleFindFurthest() {
		
		float furthest = -1.0f;
		const int size = squareDistances.size();
		for (int i = 0; i < size; ++i) {
			const float &distance = squareDistances[i];
			
			if (distance > furthest) {
				furthest = distance;
			}
		}
		return furthest;
	}
};


}} //end namespace VST::CPU
