//autor: Michael Ončo
#include <vst_gpu_sortandcut.h>

namespace VST {
namespace GPU {



const GLchar sortAndCutSrc[] = { R".(
layout(local_size_x=32)in;

uniform float uDelta = 0.000001f;

void main() {
	const uint localId = gl_LocalInvocationID.x;
	const uint globalId = gl_GlobalInvocationID.x;
	const uint globalSize = gl_WorkGroupSize.x * gl_NumWorkGroups.x;
	
	
	//------------------------------------------------------------------
	//set up useful constants
	//------------------------------------------------------------------


	const int siteCount = info[siteCountIndex];
	const int siteCount1 = siteCount - 1;
	
	const int edgesSize = info[estimatedEdgesIndex];
	const int auxIntSize = info[auxIntCountIndex];
	const int auxVecSize = info[auxVecCountIndex];
	const int cutVerticesSize = info[cutVerticesCountIndex];
	const int cutVerticesSize2 = cutVerticesSize * 2;
	const int auxVerticesUseSize = info[auxVerticesUseCountIndex];
	const int startingPointsSize = info[startingPointsCountIndex];
	const int usedIndicesSize = info[usedIndicesCountIndex];
	const int vertexMappingSize = info[vertexMappingCountIndex];
	const int vertexDistancesSize = info[vertexDistancesCountIndex];
	
	const int auxVerticesSize = info[auxVerticesCountIndex];
	const int usedVerticesSize = info[usedVerticesCountIndex];
	
	const int edgesOffset = int(globalId) * edgesSize;
	const int siteDistancesOffset = int(globalId) * siteCount;
	const int vertexDistancesOffset = int(globalId) * vertexDistancesSize;
	
	const int auxIntOffset = int(globalId) * auxIntSize;
	const int auxVecOffset = int(globalId) * auxVecSize;
	
	const int cutVerticesOffset = auxIntOffset;
	const int auxVerticesUseOffset = cutVerticesSize2 + cutVerticesOffset;
	const int startingPointsOffset = auxVerticesUseSize + auxVerticesUseOffset;
	const int usedIndicesOffset = startingPointsSize + startingPointsOffset;
	const int vertexMappingOffset = usedIndicesSize + usedIndicesOffset;
	
	const int auxVerticesOffset = auxVecOffset;
	const int usedVerticesOffset = auxVerticesSize + auxVerticesOffset;
	


	//------------------------------------------------------------------
	//initialize memory for the whole shader
	//------------------------------------------------------------------
	


	float tetrahedronSize = info[tetrahedronSizeIndex];
	const vec4 initialVertices[4] = {
		vec4(0,0,0,1),
		vec4(tetrahedronSize*0.1f,0,0,1),
		vec4(0,tetrahedronSize*0.1f,0,1),
		vec4(0,0,tetrahedronSize*0.1f,1),
	};
	
	const ivec4 initialEdges[6] = {
		ivec4(0,1,0,1),
		ivec4(0,2,0,2),
		ivec4(0,3,1,2),
		ivec4(1,2,0,3),
		ivec4(1,3,1,3),
		ivec4(2,3,2,3),
	};
	
	for (int i = 0; i < usedVerticesSize; ++i)   { auxVecBuffer[usedVerticesOffset+i] = vec4(0,0,0,1); }
	for (int i = 0; i < startingPointsSize; ++i) { auxIntBuffer[startingPointsOffset+i] = -1; }
	for (int i = 0; i < vertexMappingSize; ++i)  { auxIntBuffer[vertexMappingOffset+i] = -1; }
	for (int i = 0; i < usedIndicesSize; ++i)    { auxIntBuffer[usedIndicesOffset+i] = -1; }
	

	//------------------------------------------------------------------
	//start the main loop and initialize data for each site
	//------------------------------------------------------------------


	for (uint siteId = globalId; siteId < siteCount; siteId += globalSize) {
		vec3 mySite = sites[siteId].xyz;
		
		int edgeCount = 6;
		int vertexCount = 4;
		int indexCount = 0;
		int faceCount = 4;
		
		for (int i = 0; i < 4; ++i) {
			auxVecBuffer[auxVerticesOffset+i] = initialVertices[i];
		}
		
		for (int i = 0; i < 6; ++i) {
			edges[edgesOffset+i] = initialEdges[i];
		}
		for (int i = 6; i < edgesSize; ++i) {
			edges[edgesOffset+i] = ivec4(0,0,-1,-1);
		}
		
		for (int i = 0; i < siteCount; ++i) {
			const vec3 site = sites[i].xyz;
			const vec3 distanceVector = site - mySite;
			siteDistances[siteDistancesOffset+i] = dot(distanceVector, distanceVector);
		}
		siteDistances[siteDistancesOffset+siteId] = -1;
		
		for (int i = 0; i < 4; ++i) {
			const vec3 distanceVector = mySite.xyz - auxVecBuffer[auxVerticesOffset+i].xyz;
			vertexDistances[vertexDistancesOffset+i] = dot(distanceVector, distanceVector);
		}
		for (int i = 4; i < vertexDistancesSize; ++i) {
			vertexDistances[vertexDistancesOffset+i] = -1;
		}
		
		
		bool continueCutting = true;
		float lastClosest = 0;
		float furthestVertex = 9999.0f;
		int cuttingIteration = 0;


		//------------------------------------------------------------------
		//start the cutting loop
		//------------------------------------------------------------------

		
//		while (continueCutting && cuttingIteration < siteCount1) {
		while (cuttingIteration < siteCount1) {
			

			//------------------------------------------------------------------
			//find the next closest to use for cutting and furthest cell vertex
			//------------------------------------------------------------------


			int closestIndex = 0;
			float closestDistance = siteDistances[siteDistancesOffset+0];
			for (int i = 0; i < siteCount; ++i) {
				const float distance = siteDistances[siteDistancesOffset+i];
				if ((closestDistance <= lastClosest || distance < closestDistance) && distance > lastClosest) {
					closestDistance = distance;
					closestIndex = i;
				}
			}
			
			lastClosest = closestDistance;
			
			furthestVertex = vertexDistances[vertexDistancesOffset+0];
			for (int i = 1; i < vertexCount; ++i) {
				float distance = vertexDistances[vertexDistancesOffset+i];
				if (distance > furthestVertex) furthestVertex = distance;
			}
			
			
//			if (closestDistance / 2.0f > furthestVertex) {
			if (closestDistance / 4.0f > furthestVertex) {
				continueCutting = false;
				break;
			}
			


			//------------------------------------------------------------------
			//criterion didn't end the cutting, initialize data for cutting
			//------------------------------------------------------------------



//			test0[siteDistancesOffset+cuttingIteration] = vec4(sites[closestIndex].xyz, closestDistance);
			vec3 closestSite = sites[closestIndex].xyz;
			vec3 planePoint = (closestSite + mySite) * 0.5f;
			vec3 planeNormal = normalize(closestSite - mySite);
			float planeConst = -(dot(planePoint, planeNormal));
			vec4 plane = vec4(planeNormal, planeConst);
			
			for (int i = 0; i < cutVerticesSize2; i+=2) {
				auxIntBuffer[cutVerticesOffset+i+0] = -1;
				auxIntBuffer[cutVerticesOffset+i+1] = -1;
			}
			

			//------------------------------------------------------------------
			//test each edge for cutting
			//------------------------------------------------------------------

			
			for (int edgeIndex = 0; edgeIndex < edgeCount; ++edgeIndex) {
				int outside = 0;
				const ivec4 edge = edges[edgesOffset+edgeIndex];
				if (edge.x == edge.y) continue;
				vec4 linePoints[2] = {
					auxVecBuffer[auxVerticesOffset+edge[0]], 
					auxVecBuffer[auxVerticesOffset+edge[1]]
				};
				

				//------------------------------------------------------------------
				//determine if cutting edges is necessary
				//------------------------------------------------------------------


				const float pointOrientation[2] = {
					dot(plane, linePoints[0]),
					dot(plane, linePoints[1])
				};
				
				outside |= int(pointOrientation[0] > uDelta) << 0;
				outside |= int(pointOrientation[1] > uDelta) << 1;
				
				bool needsCutting = false;
				int inIndex = 0;
				int outIndex = 0;
				
				switch (outside) {
				case 0:
					break;
				case 1:
					inIndex = 1;
					outIndex = 0;
					needsCutting = true;
					break;
				case 2:
					inIndex = 0;
					outIndex = 1;
					needsCutting = true;
					break;
				case 3:
//					auxIntBuffer[auxVerticesUseOffset+edges[edgesOffset+edgeIndex][outIndex]]--;
//					auxIntBuffer[auxVerticesUseOffset+edges[edgesOffset+edgeIndex][outIndex]]--;
					vertexDistances[vertexDistancesOffset+edges[edgesOffset+edgeIndex].x] = -1;
					vertexDistances[vertexDistancesOffset+edges[edgesOffset+edgeIndex].y] = -1;
					edges[edgesOffset+edgeIndex] = ivec4(0,0,-1,-1);
					break;
				default:
					break;
				}
				

				//------------------------------------------------------------------
				//cut edge if necessary and create new vertex
				//------------------------------------------------------------------


				if (needsCutting) {
					const vec4 inVertex = linePoints[inIndex];
					const vec4 outVertex = linePoints[outIndex];
					const vec4 dir = inVertex - outVertex;
					const vec4 newVertex = (dot(-plane, inVertex) / dot(plane, dir) * dir) + inVertex;
					
					
					const int newVertexIndex = vertexCount;
//					auxIntBuffer[auxVerticesUseOffset+newVertexIndex] = 1;
					auxVecBuffer[auxVerticesOffset+newVertexIndex] = newVertex;
					const vec3 distanceVector = mySite.xyz - newVertex.xyz;
					vertexDistances[vertexDistancesOffset+newVertexIndex] = dot(distanceVector, distanceVector);
					++vertexCount;
					
					
//					auxIntBuffer[auxVerticesUseOffset+edges[edgesOffset+edgeIndex][outIndex]]--;
					vertexDistances[vertexDistancesOffset+edges[edgesOffset+edgeIndex][outIndex]] = -1;
					edges[edgesOffset+edgeIndex][outIndex] = newVertexIndex;
					
					const int cutVertexZ = cutVerticesOffset+edge.z*2;
					const int cutVertexW = cutVerticesOffset+edge.w*2;
					
					if      (auxIntBuffer[cutVertexZ+0] == -1) auxIntBuffer[cutVertexZ+0] = newVertexIndex;
					else if (auxIntBuffer[cutVertexZ+1] == -1) auxIntBuffer[cutVertexZ+1] = newVertexIndex;
					if      (auxIntBuffer[cutVertexW+0] == -1) auxIntBuffer[cutVertexW+0] = newVertexIndex;
					else if (auxIntBuffer[cutVertexW+1] == -1) auxIntBuffer[cutVertexW+1] = newVertexIndex;
					
					
					
				}
			}
			

			//------------------------------------------------------------------
			//connect cut face with new edge
			//------------------------------------------------------------------


			const int newFaceIndex = faceCount++;
//			for (int i = 0; i < 50; ++i) {
			for (int i = 0; i < faceCount; ++i) {
				const int newEdgeIndex = edgeCount++;
				if (auxIntBuffer[cutVerticesOffset+i*2+0] != auxIntBuffer[cutVerticesOffset+i*2+1] 
				&&  auxIntBuffer[cutVerticesOffset+i*2+0] != -1 
				&&  auxIntBuffer[cutVerticesOffset+i*2+1] != -1) {
					edges[edgesOffset+newEdgeIndex] = 
						ivec4
							(auxIntBuffer[cutVerticesOffset+i*2+0]
							,auxIntBuffer[cutVerticesOffset+i*2+1]
							,i
							,newFaceIndex);
				}
			}
			
			
			++cuttingIteration;
		}
		
		
		//------------------------------------------------------------------
		//initialize for creating triangles
		//------------------------------------------------------------------

		int usedVerticesCount = 0;
		int usedIndexCount = 0;
		
		for (int i = 0; i < usedVerticesSize; ++i)   { auxVecBuffer[usedVerticesOffset+i] = vec4(0,0,0,1); }
		for (int i = 0; i < startingPointsSize; ++i) { auxIntBuffer[startingPointsOffset+i] = -1; }
		for (int i = 0; i < vertexMappingSize; ++i)  { auxIntBuffer[vertexMappingOffset+i] = -1; }
		for (int i = 0; i < usedIndicesSize; ++i)    { auxIntBuffer[usedIndicesOffset+i] = -1; }
		
		for (int i = 0; i < edgeCount; ++i) {
			ivec4 edge = edges[edgesOffset+i];
//			edges[edgesOffset+i] = ivec4(0,0,-1,-1);
			if (edge.x == edge.y) continue;
			int startingPoint1 = auxIntBuffer[startingPointsOffset+edge.z];
			int startingPoint2 = auxIntBuffer[startingPointsOffset+edge.w];
			int vertexMapping1 = auxIntBuffer[vertexMappingOffset+edge.x];
			int vertexMapping2 = auxIntBuffer[vertexMappingOffset+edge.y];
			
			
			if (vertexMapping1 == -1) {
				auxVecBuffer[usedVerticesOffset+usedVerticesCount] = auxVecBuffer[auxVerticesOffset+edge.x];
				vertexMapping1 = usedVerticesCount++;
				auxIntBuffer[vertexMappingOffset+edge.x] = vertexMapping1;
			}
			if (vertexMapping2 == -1) {
				auxVecBuffer[usedVerticesOffset+usedVerticesCount] = auxVecBuffer[auxVerticesOffset+edge.y];
				vertexMapping2 = usedVerticesCount++;
				auxIntBuffer[vertexMappingOffset+edge.y] = vertexMapping2;
			}
			
			if (startingPoint1 == -1) {
				auxIntBuffer[startingPointsOffset+edge.z] = edge.x;
			} else if(startingPoint1 != edge.x && startingPoint1 != edge.y) {
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = auxIntBuffer[vertexMappingOffset+startingPoint1];
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = vertexMapping1;
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = vertexMapping2;
			}
			if (startingPoint2 == -1) {
				auxIntBuffer[startingPointsOffset+edge.w] = edge.x;
			} else if(startingPoint2 != edge.x && startingPoint2 != edge.y) {
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = auxIntBuffer[vertexMappingOffset+startingPoint2];
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = vertexMapping1;
				auxIntBuffer[usedIndicesOffset+usedIndexCount++] = vertexMapping2;
			}
			
			
		}
		
		//------------------------------------------------------------------
		//load up new triangles and commands into the rendering buffers and save statistics
		//------------------------------------------------------------------

		
		atomicMax(info[test0Index], edgeCount);
		atomicMax(info[test4Index], vertexCount);
		
		//allocate space for indices and vertices
		//copy indices and vertices
		const int indicesOffset = atomicAdd(info[indexFreeSpaceIndex], usedIndexCount);
		const int verticesOffset = atomicAdd(info[vertexFreeSpaceIndex], usedVerticesCount);
		
		
		for (int i = 0; i < usedIndexCount; ++i) {
			indices[indicesOffset+i] = auxIntBuffer[usedIndicesOffset+i];
		}
		
		for (int i = 0; i < usedVerticesCount; ++i) {
			vertices[verticesOffset+i] = auxVecBuffer[usedVerticesOffset+i];
		}
		
		
		
		const int commandOffset = int(siteId) * 5;
		commands[commandOffset+0] = usedIndexCount; // number of indices to render;
		commands[commandOffset+1] = 1;              // instance to render
		commands[commandOffset+2] = indicesOffset;  // offset of first index;
		commands[commandOffset+3] = verticesOffset; // offset of first vertex;
		commands[commandOffset+4] = int(siteId);    // first and only instance id
		
		test0[siteId] = cuttingIteration;
		atomicAdd(info[test1Index], cuttingIteration);
		if (cuttingIteration == siteCount - 1)
			atomicAdd(info[test2Index], 1);
		if (cuttingIteration == 1)
			atomicAdd(info[test3Index], 1);
		
	}
}

)." };


}} //end namespace VST::GPU


