//autor: Michael Ončo
#pragma once

#include <limits>

#include <vector>
#include <glm/glm.hpp>
#include <stdio.h>

namespace VST {
namespace CPU {

struct SiteSortingInfo {
	float lastClosest;
	
	const std::vector<glm::vec4> *usedSites;
	
	std::vector<float> squareDistances;
	
	void init(const std::vector<glm::vec4> &sites, const int siteIndex) {
		const int size = sites.size();
		usedSites = &sites;
		lastClosest = -1.0f;
		squareDistances.clear();
		
		squareDistances.reserve(size);
		
		const glm::vec4 &cellSite = sites[siteIndex];
		
		for (int i = 0; i < size; ++i) {
			const glm::vec4 distanceVector = cellSite - sites[i];
			squareDistances.push_back(glm::dot(distanceVector, distanceVector));
		}
		
		glm::vec4 aux;
		simpleFindClosest(aux);
	}
	
	float simpleFindClosest(glm::vec4 &site) {
		const int size = usedSites->size();
		float closest = std::numeric_limits<float>::infinity();
		int closestIndex = -1;
		
		
		for (int i = 0; i < size; ++i) {
			const float distance = squareDistances[i];
			if (lastClosest < distance && closest > distance) {
				closest = distance;
				closestIndex = i;
			}
		}
		
		if (closestIndex == -1) {
			site = glm::vec4(0,0,0,0);
		} else {
			site = (*usedSites)[closestIndex];
			lastClosest = closest;
		}
		
		return closest;
	}
};

}} //end namespace VST::CPU

