
#include <vst_gpu_cutcells.h>

namespace VST {
namespace GPU {

const GLchar placeholderCommandLoaderSrc[] = { R".(
layout(local_size_x=32)in;

void main() {
	vec4 testVertices[] = {{0,0,0,1}, {1,0,0,1}, {0,1,0,1}, {0,0,1,1}};
	int testIndices[] = {
		0,1,2,
		0,1,3,
		0,2,3,
		1,2,3,
	};
	
//		command = {indexCount, 1, indexTotal, 0, index};
	int testCommands[] = {
		12,1,0,0,int(true)
	};
	
	for (int i = 0; i < 4; ++i) { vertices[i] = testVertices[i]; }
	for (int i = 0; i < 12; ++i) { indices[i] = testIndices[i]; }
	for (int i = 0; i < 5; ++i) { commands[i] = testCommands[i]; }
	offsets[0] = vec4(0.01f,0.01f,0.01f,0);
}
)." };



//TODO udelat to at mam nejaky fixnejsi mnozstvi mista v pameti - budu mit napriklad 100 pracovnich skupin a misto v pameti se bude vztahovat vzdy ke skupine misto toho aby se vztahoval k poctu bodu... 
//TODO dale by alokovani mista pro vertexy a indexy mohlo byt doresene docela fajn tak ze bych si misto rozdeloval po 10 polozkach a mel bych nejakou sdilenou promennou ve ktere by si kazdy pekne zapisoval kolik mista potrebuje... 
//TODO takze by bylo vzdy max 9 polozek nevyuzitych max na jednu bunku
const GLchar cutCellsSrc[] = { R".(
layout(local_size_x=32)in;

uniform float uDelta = 0.000001f;

void main() {
	uint x = gl_GlobalInvocationID.x;
	uint siteCount = info[siteCountIndex];
	uint workGroupSize = gl_WorkGroupSize.x;
	uint workGroupId = gl_WorkGroupID.x;
	
	
	
	for (uint id = x; id < siteCount; id += workGroupSize) {
//	for (uint id = x; id < 2; id += workGroupSize) {
//		id = 1;
		int cuttingIteration = 1;
		float furthestVertex = 9999.0f;
		vec3 mySite = sites[id].xyz;
		
		const vec4 boundingSites[4] = {
			vec4(0,0,0,1),
			vec4(0,0,0,1),
			vec4(0,0,0,1),
			vec4(0,0,0,1),
		};
		
/*		const vec4 initialVertices[4] = {
			vec4(0,0,0,1),
			vec4(10,0,0,1),
			vec4(0,10,0,1),
			vec4(0,0,10,1),
		};
*/		
		
		const ivec4 initialEdges[6] = {
			ivec4(0,1,0,1),
			ivec4(0,2,0,2),
			ivec4(0,3,1,2),
			ivec4(1,2,0,3),
			ivec4(1,3,1,3),
			ivec4(2,3,2,3),
		};
		
		const int verticesOffset = int(id) * info[estimatedVerticesIndex];// * 4;
		const int edgesOffset = int(id) * info[estimatedEdgesIndex];
		const int indicesOffset = int(id) * info[estimatedIndicesIndex];
		const int sortedSitesOffset = int(id * siteCount);
		
		for (int i = 0; i < 4; ++i) {
			
			vertices[verticesOffset+i] = vertices[i];
//this could be also done by copying from the first site
//			vertices[verticesOffset+i] = initialVertices[i];
		}
		
		for (int i = 0; i < 6; ++i) {
			edges[edgesOffset+i] = initialEdges[i];
		}
		
		
		offsets[id] = vec4(sites[id].xyz, 0);
		int edgeCount = 6;
		int vertexCount = 4;
		int indexCount = 0;
		int faceCount = 4;
		
//		const int edgeOffset = id * info[estimatedEdgesIndex];
		
		
		
		bool continueCutting = true;
//		while (continueCutting) {
		for (;cuttingIteration < 11;) {
			vec3 closestSite = sortedSites[sortedSitesOffset+cuttingIteration].xyz;
			
			vec3 planePoint = (closestSite + mySite) * 0.5f;
			vec3 planeNormal = normalize(closestSite - mySite);
			float planeConst = -(dot(planePoint, planeNormal));
			vec4 plane = vec4(planeNormal, planeConst);
			
			ivec2 cutVertices[50];
			for (int i = 0; i < 50; ++i) cutVertices[i] = ivec2(-1,-1);
			
			//resit hrany po davkach by se hodilo
			
			for (int edgeIndex = 0; edgeIndex < edgeCount; ++edgeIndex) {
				int outside = 0;
				const ivec4 edge = edges[edgesOffset+edgeIndex];
				vec4 linePoints[2] = {vertices[verticesOffset+edge[0]], vertices[verticesOffset+edge[1]]};
				
				const float pointOrientation[2] = {
					dot(plane, linePoints[0]),
					dot(plane, linePoints[1])
				};
				
				outside |= int(pointOrientation[0] > uDelta) << 0;
				outside |= int(pointOrientation[1] > uDelta) << 1;
				
				bool needsCutting = false;
				int inIndex = 0;
				int outIndex = 0;
				
				switch (outside) {
				case 0:
					break;
				case 1:
					inIndex = 1;
					outIndex = 0;
					needsCutting = true;
					break;
				case 2:
					inIndex = 0;
					outIndex = 1;
					needsCutting = true;
					break;
				case 3:
					edges[edgesOffset+edgeIndex] = ivec4(0,0,-1,-1);
					break;
					//nastav stary edge jako prazdy - idealne zapamatuj at se to da potom znova pouzit - nejaky buffer vlastni s tim by byl treba...???
				default:
					break;
				}
				
				if (needsCutting) {
					const vec4 inVertex = linePoints[inIndex];//vertices[verticesOffset+edge[inIndex]];
					const vec4 outVertex = linePoints[outIndex];//vertices[verticesOffset+edge[outIndex]];
					const vec4 dir = inVertex - outVertex;
					const vec4 newVertex = (dot(-plane, inVertex) / dot(plane, dir) * dir) + inVertex;
					
					
					//uloz novy bod na spravne misto - jak toto? proste mit counter
					
					const int newVertexIndex = vertexCount;
					vertices[verticesOffset+newVertexIndex] = newVertex;
					++vertexCount;
					
					
					//nahrad stary bod novym
					//stare body nejak rusit?
					edges[edgesOffset+edgeIndex][outIndex] = newVertexIndex;
					
					if (cutVertices[edge.z].x == -1) cutVertices[edge.z].x = newVertexIndex;
					else if (cutVertices[edge.z].y == -1) cutVertices[edge.z].y = newVertexIndex;
					
					if (cutVertices[edge.w].x == -1) cutVertices[edge.w].x = newVertexIndex;
					else if (cutVertices[edge.w].y == -1) cutVertices[edge.w].y = newVertexIndex;
					
					//updatuj informace o nejvzdalenejsim bodu?
					//zapamatuj si novy bod pro vytvoreni nove steny
					
					
				}
				
			}
			
			//connect cut face
			const int newFaceIndex = faceCount++;
			for (int i = 0; i < 50; ++i) {
				const int newEdgeIndex = edgeCount++;
				if (cutVertices[i].x != cutVertices[i].y && cutVertices[i].x != -1 && cutVertices[i].y != -1) {
					edges[edgesOffset+newEdgeIndex] = ivec4(cutVertices[i].x, cutVertices[i].y, i, newFaceIndex);
				}
			}
			
			
			++cuttingIteration;
		}
		
		
		//vytvareni indexu a commandu by asi mohlo byt vice paralelizovano... tim by se mohlo dat zamezit vysokym narokum na pamet a neznamemu poctu prvku ve startingPoints
		int startingPoints[50];
		for (int i = 0; i < 50; ++i) startingPoints[i] = -1;
		for (int i = 0; i < edgeCount; ++i) {
			ivec4 edge = edges[edgesOffset+i];
			if (edge.x == edge.y) continue;
			int startingPoint1 = startingPoints[edge.z];
			int startingPoint2 = startingPoints[edge.w];
			
			if (startingPoint1 == -1) {
				startingPoints[edge.z] = edge.x;
			} else if(startingPoint1 != edge.x && startingPoint1 != edge.y) {
				//create indices
				indices[indicesOffset+indexCount++] = startingPoint1;
				indices[indicesOffset+indexCount++] = edge.x;
				indices[indicesOffset+indexCount++] = edge.y;
			}
			if (startingPoint2 == -1) {
				startingPoints[edge.w] = edge.x;
			} else if(startingPoint2 != edge.x && startingPoint2 != edge.y) {
				//create indices
				indices[indicesOffset+indexCount++] = startingPoint2;
				indices[indicesOffset+indexCount++] = edge.x;
				indices[indicesOffset+indexCount++] = edge.y;
			}
			
			
		}
		
		
//		command = {indexCount, 1, indexTotal, 0, index};
		const int commandOffset = int(id) * 5;
//		const int commandOffset = 0 * 5;
		commands[commandOffset+0] = indexCount;// number of indices to render;
		commands[commandOffset+1] = 1;// instance to render
		commands[commandOffset+2] = indicesOffset;// offset of first index;
		commands[commandOffset+3] = verticesOffset;// offset of first vertex;
		commands[commandOffset+4] = int(id);// first and only instance id
	}
}
)." };

}} //end namespace VST::GPU

