//autor: Michael Ončo

#include <GL/glew.h>

#include <mainwindow.h>

using namespace OGL;



namespace Main {

bool windowInitialized = false;
GLWindow *window = nullptr;

//------------------------------------------------------------------
//set up shaders and programs
//------------------------------------------------------------------


Program *windowWireProgram = nullptr;
Program *windowPointProgram = nullptr;
Program *windowVoronoiProgram = nullptr;
Program *windowProgram = nullptr;

Shader *windowVShader = nullptr;
Shader *windowGShader = nullptr;
Shader *windowFShader = nullptr;
Shader *windowVoronoiVShader = nullptr;
Shader *windowVoronoiGShader = nullptr;
Shader *windowVoronoiFShader = nullptr;
Shader *windowWireFShader = nullptr;
Shader *windowPointFShader = nullptr;

Buffer *windowVertexBuffer = nullptr;
Buffer *windowIndexBuffer = nullptr;

VertexArray *windowVAO = nullptr;

OrbitManipulator *windowOrbitManipulator = nullptr;


//------------------------------------------------------------------
//shader -generic code
//------------------------------------------------------------------

//rendering shaders - mostly unused for now
static GLchar vSrc[] = { R".(
#version 450

layout(location=0) in vec4 coord;

uniform mat4 VP = mat4(1.0f);

void main() {
	gl_Position = VP * coord;
	gl_PointSize = gl_Position.z == 0 ? 0.0f : 100.0f / gl_Position.z;
}
)." };

static GLchar gSrc[] = { R".(
#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

out vec4 gNormal;

void main() {

	vec3 v1 = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
	vec3 v2 = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
	vec4 normal = vec4(normalize(cross(v2, v1)), 0);

	gl_Position = gl_in[0].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[1].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[2].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	EndPrimitive();
}
)." };

static GLchar fSrc[] = { R".(
#version 450

out vec4 fColor;

in vec4 gNormal;

uniform vec4 uDirection = vec4(0,0,1,0);
uniform vec4 uColor = vec4(0.1,1,1,1);
uniform float uAmbient = 0.1f;

void main() {
	vec4 color = uColor;
	vec4 direction = normalize(uDirection);
	float diffuse = dot(gNormal, direction);

	fColor = color * clamp((diffuse + uAmbient), 0, 1);
}
)." };


//------------------------------------------------------------------
//voronoi shader code for indirect rendering
//------------------------------------------------------------------


static GLchar voronoiVSrc[] = { R".(
#version 450

#extension GL_ARB_shader_draw_parameters : require

layout(location=0) in vec4 coord;

layout(binding=0)buffer Offsets{vec4 offsets[];};

uniform mat4 uVP = mat4(1.0f);
uniform float uCellDistance = 0.0f;

out int vInstanceID;
out vec4 vSite;

void main() {
	vInstanceID = gl_BaseInstanceARB + gl_InstanceID;
	vec4 offsetVector = vec4(offsets[vInstanceID].xyz, 0);
	vec4 site = vec4(offsets[vInstanceID].xyz, 1);
//		vOffset = uVP * vec4(offsets[vInstanceID].xyz, 1);
//		gl_Position = uVP * (coord + offsets[vInstanceID] * uCellDistance);
	vSite = uVP * (site + offsetVector * uCellDistance);
	gl_Position = uVP * (coord + offsetVector * uCellDistance);
	gl_PointSize = gl_Position.z == 0 ? 0.0f : 100.0f / gl_Position.z;
}
)." };

static GLchar voronoiGSrc[] = { R".(
#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in int vInstanceID[];
in vec4 vSite[];

out int gInstanceID;
out vec4 gNormal;

void main() {
	gInstanceID = vInstanceID[0];

	vec3 v1 = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
	vec3 v2 = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
	vec4 normal = vec4(normalize(cross(v2, v1)), 0);

	//TODO make sure this works right
	if (dot(normal, normalize(gl_in[0].gl_Position - vSite[0])) > 0) normal = -normal;
//		if (dot(normal, normalize(gl_in[0].gl_Position - vSite[0])) > 0) normal = -normal;

	gl_Position = gl_in[0].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[1].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[2].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	EndPrimitive();
}
)." };

static GLchar voronoiFSrc[] = { R".(
#version 450

out vec4 fColor;

flat in int gInstanceID;
in vec4 gNormal;

uniform vec4 uDirection = vec4(0,0,1,0);
uniform vec4 uColor = vec4(0.1,1,1,1);
uniform float uAmbient = 0.1f;

void main() {
	vec4 color = uColor;
	vec4 direction = normalize(uDirection);
	float diffuse = dot(gNormal, direction);

	fColor = color * clamp((diffuse + uAmbient), 0, 1);
}
)." };


//------------------------------------------------------------------
//simple shaders for wireframes and points
//------------------------------------------------------------------



static GLchar pointFSrc[] = { R".(
#version 450

out vec4 fColor;

void main() {
	fColor = vec4(0.1,1,1,1);
}
)." };

static GLchar wireFSrc[] = { R".(
#version 450

out vec4 fColor;

void main() {
	fColor = vec4(0.1,1,1,1);
}
)." };



void runWindow() {
	assert(window != nullptr);
	window->run();
}




//------------------------------------------------------------------
//initialize all relevant data so that if anyone wants to use any of the shaders or buffers, they can
//------------------------------------------------------------------


void initializeWindow() {



//	window = new GLWindow(1920,1080);
	window = new GLWindow;

	windowWireProgram = new Program;
	windowPointProgram = new Program;
	windowVoronoiProgram = new Program;
	windowProgram = new Program;
	windowVShader = new Shader(GL_VERTEX_SHADER, vSrc);
	windowGShader = new Shader(GL_GEOMETRY_SHADER, gSrc);
	windowFShader = new Shader(GL_FRAGMENT_SHADER, fSrc);

	windowVoronoiVShader = new Shader(GL_VERTEX_SHADER, voronoiVSrc);
	windowVoronoiGShader = new Shader(GL_GEOMETRY_SHADER, voronoiGSrc);
	windowVoronoiFShader = new Shader(GL_FRAGMENT_SHADER, voronoiFSrc);

	windowWireFShader = new Shader(GL_FRAGMENT_SHADER, wireFSrc);
	windowPointFShader = new Shader(GL_FRAGMENT_SHADER, pointFSrc);

	windowWireProgram->attachShader(windowVShader);
	windowWireProgram->attachShader(windowWireFShader);

	windowPointProgram->attachShader(windowVShader);
	windowPointProgram->attachShader(windowPointFShader);

	windowVoronoiProgram->attachShader(windowVoronoiVShader);
	windowVoronoiProgram->attachShader(windowVoronoiGShader);
	windowVoronoiProgram->attachShader(windowVoronoiFShader);

	windowProgram->attachShader(windowVShader);
	windowProgram->attachShader(windowGShader);
	windowProgram->attachShader(windowFShader);

	windowProgram->link();
	windowVoronoiProgram->link();
	windowWireProgram->link();
	windowPointProgram->link();

//	windowVertexBuffer = new Buffer(GL_STATIC_DRAW);
//	windowIndexBuffer = new Buffer(GL_STATIC_DRAW);
	
	windowOrbitManipulator = new OrbitManipulator();
	window->addInputFunction(windowOrbitManipulator->inputFunction);

	//TODO this used data from gdel_algorithms... which is not right
//	int testVertices[1000];
//	int testIndices[1000];

//	windowVertexBuffer->bind(GL_ARRAY_BUFFER);
	//vertexBuffer->set(16 * sizeof(float), &vData);
//	windowVertexBuffer->createMutable(4 * 10 * sizeof(float), (void *)testVertices);

//	windowIndexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	//indexBuffer->set(9 * sizeof(int), &iData);
//	windowIndexBuffer->createMutable(3 * 10 * sizeof(unsigned int), (void *)testIndices);


	windowVAO = new VertexArray;
	windowVAO->bind();
//	windowVAO->addAttribute(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);

	glClearColor(0.2, 0.2, 0.2, 1);

	glEnable(GL_PROGRAM_POINT_SIZE);


	window->addInputFunction([&](SDL_Event &e) -> int {
		unsigned char key;
		switch (e.type) {
		case SDL_KEYDOWN:
			key = e.key.keysym.sym;

			switch (key) {
			case SDLK_q:
				return 1;
				break;
			}
			break;
		}
		return 0;
	});
	
	windowInitialized = true;
}





void destroyWindow() {
	delete window;
	

	delete windowWireProgram;
	delete windowPointProgram;
	delete windowVoronoiProgram;
	delete windowProgram;

	delete windowVShader;
	delete windowGShader;
	delete windowFShader;
	delete windowPointFShader;
	delete windowWireFShader;

	delete windowVertexBuffer;
	delete windowIndexBuffer;

	delete windowVAO;

}





void initializeWindowRender() {
	window->addRenderFunction([&]() -> int {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		windowProgram->useProgram();
		windowProgram->uniformMat4("VP", windowOrbitManipulator->matrix);
		windowVAO->bind();
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		windowVertexBuffer->bind(GL_ARRAY_BUFFER);
		windowIndexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
		glDrawElements(GL_TRIANGLES, 30, GL_UNSIGNED_INT, 0);
		glDrawArrays(GL_POINTS, 0, 40);
		return 0;
	});
}

} //end namespace Main

