//autor: Michael Ončo
#include <vst_gpu.h>

#include <vector>
#include <chrono>

#include <buffer.h>
#include <shader.h>
#include <program.h>

#include <mainwindow.h>
#include <vst_gpu_sortandcut.h>

#include <glm/glm.hpp>
#include <GL/glew.h>

using namespace OGL;

namespace VST {
namespace GPU {


//------------------------------------------------------------------
//memory constants, buffers and shaders
//------------------------------------------------------------------


static float delta = 0.00001f;

static int renderSize = 0;
static float cellDistance = 0.0f;

static float sensitivity = 0.01f;
static bool rightMouseDown = false;

static const int estimatedVertexCount = 100;  //controls the amount of memory dedicated to final vertices per site
static const int estimatedIndexCount = 300;   //controls the amount of memory dedicated to final indices per site
static const int estimatedEdgeCount = 100000; //controls the amount of memory dedicated to edges used per thread

static int tetrahedronSize = 10; //divided by 10 for the actual size of tetrahedron
static float siteRange = 1.0f;

//static int const groupCount = 8;
//static int siteCount = 1200;
static int totalShaderCount = 32;


static const int auxVerticesCount = 1000;
static const int auxVerticesUseCount = auxVerticesCount;
static const int cutVerticesCount = 1000;

static const int startingPointsCount = 100;
static const int usedIndicesCount = 300;
static const int usedVerticesCount = 100;
static const int vertexMappingCount = auxVerticesCount;
static const int vertexDistancesCount = auxVerticesCount;

static const int estimatedAuxVecCount = 
	(auxVerticesCount
	+usedVerticesCount
	);
static const int estimatedAuxIntCount = 
	(startingPointsCount 
	+cutVerticesCount * 2 
	+usedIndicesCount
	+vertexMappingCount
	+auxVerticesUseCount
	);


//static Shader *sortingShader = nullptr;
//static Shader *cuttingShader = nullptr;
static Shader *sortAndCutShader = nullptr;

static Program *vstProgram = nullptr;

static Buffer *infoBuffer = nullptr;
static Buffer *siteBuffer = nullptr;
static Buffer *vertexBuffer = nullptr;
static Buffer *edgeBuffer = nullptr;
static Buffer *siteDistancesBuffer = nullptr;
static Buffer *indexBuffer = nullptr;
static Buffer *commandBuffer = nullptr;
static Buffer *vertexDistancesBuffer = nullptr;

static Buffer *auxIntBuffer = nullptr;
static Buffer *auxVecBuffer = nullptr;

static Buffer *test0Buffer = nullptr;





//------------------------------------------------------------------
//shader header for easiesr access
//------------------------------------------------------------------


static const GLchar sortAndCutHeaderSrc[] = { R".(
#version 430

layout(binding=0)buffer Info{int info[];};
layout(binding=1)buffer Sites{vec4 sites[];};
layout(binding=2)buffer Vertices{vec4 vertices[];};
layout(binding=3)buffer Edges{ivec4 edges[];};
layout(binding=4)buffer SiteDistances{float siteDistances[];};
layout(binding=5)buffer Indices{int indices[];};
layout(binding=6)buffer Commands{int commands[];};
layout(binding=7)buffer VertexDistances{float vertexDistances[];};
layout(binding=8)buffer AuxVecBuffer{vec4 auxVecBuffer[];};
layout(binding=9)buffer AuxIntBuffer{int auxIntBuffer[];};

layout(binding=19)buffer Test0{int test0[];};


const int siteCountIndex = 0;
const int estimatedEdgesIndex = 1;
const int estimatedVerticesIndex = 2;
const int estimatedIndicesIndex = 3;

const int indexFreeSpaceIndex = 4;
const int vertexFreeSpaceIndex = 5;

const int auxIntCountIndex = 6;
const int auxVecCountIndex = 7;

const int cutVerticesCountIndex = 8;
const int startingPointsCountIndex = 9;
const int usedIndicesCountIndex = 10;
const int vertexMappingCountIndex = 12;
const int auxVerticesUseCountIndex = 13;
const int vertexDistancesCountIndex = 15;

const int usedVerticesCountIndex = 11;
const int auxVerticesCountIndex = 14;

const int test0Index = 16;
const int test1Index = 17;
const int test2Index = 18;
const int test3Index = 19;
const int test4Index = 20;

const int tetrahedronSizeIndex = 22;
)." };





void destroyVer2() {
	delete infoBuffer;
	delete siteBuffer;
	delete vertexBuffer;
	delete edgeBuffer;
	delete indexBuffer;
	delete commandBuffer;
	delete siteDistancesBuffer;
	delete vertexDistancesBuffer;
	delete auxVecBuffer;
	delete auxIntBuffer;
	delete test0Buffer;
}





void initVer2(int groupCount, int siteCount) {
	totalShaderCount = totalShaderCount * groupCount;
//	printf("VST GPU 1 kernel implementation.\n");//Press ENTER to start.\n");
//	scanf("%*c");
//	printf("init\n");
	
	//------------------------------------------------------------------
	//compute memory sizes and offsets
	//------------------------------------------------------------------

	const int auxIntsByteSize = totalShaderCount * estimatedAuxIntCount * sizeof(int);
	const int auxVecsByteSize = totalShaderCount * estimatedAuxVecCount * 4 * sizeof(float);
	
	const int verticesByteSize = estimatedVertexCount * siteCount * 4 * sizeof(float);
	const int indicesByteSize = estimatedIndexCount * siteCount * sizeof(int);
	const int commandsByteSize = siteCount * 5 * sizeof(int);
	const int edgesByteSize = estimatedEdgeCount * totalShaderCount * 4 * sizeof(int);
	const int siteDistancesByteSize = siteCount * totalShaderCount * sizeof(float);
	const int vertexDistancesByteSize = vertexDistancesCount * totalShaderCount * sizeof(float);
	const int sitesByteSize = siteCount * 4 * sizeof(float);
	
	printf("calculated memory size:\n-------------------------------\n");
	printf("vertexBuffer size: %f kB\n", (float)verticesByteSize / 1024.0f); //all vertices for render
	printf("indexBuffer size: %f kB\n", (float)indicesByteSize / 1024.0f); //all indices for render
	printf("edgesBuffer size: %f kB\n", (float)edgesByteSize / 1024.0f); //first aux buffer -> edges
	printf("siteDistancesBuffer size: %f kB\n", (float)siteDistancesByteSize / 1024.0f); //site distances
	printf("vertexDistancesBuffer size: %f kB\n", (float)vertexDistancesByteSize / 1024.0f); //cell vertex distances
	printf("siteBuffer size: %f kB\n", (float)sitesByteSize / 1024.0f); //sites
	printf("commandsBuffer size: %f kB\n", (float)commandsByteSize / 1024.0f);
	printf("auxIntBuffer size: %f kB\n", (float)auxIntsByteSize / 1024.0f); //aux buffer for all manner of helpful variables
	printf("auxVecBuffer size: %f kB\n", (float)auxVecsByteSize / 1024.0f);
	printf("total: %f kB\n-------------------------------\n", (float)
		(verticesByteSize
		+indicesByteSize
		+edgesByteSize
		+siteDistancesByteSize
		+sitesByteSize
		+commandsByteSize
		+auxIntsByteSize
		+auxVecsByteSize
		) / 1024.0f);
	

	//------------------------------------------------------------------
	//create random set of points in tetrahedron
	//------------------------------------------------------------------

	
	std::vector<glm::vec4> sites;
	
	
	sites.reserve(siteCount);
	
	float (*rand)(float) = [](float limit) -> float {
		return std::rand() / float(RAND_MAX) * limit;
	};
	
	
//	glm::vec3 planeNormal = {1,1,1};
//	glm::vec3 planePoint = {1,0,0};
//	glm::vec4 tetrahedronPlane = {0.1f*tetrahedronSize,0.1f*tetrahedronSize,0.1f*tetrahedronSize,-0.1f*tetrahedronSize};
	glm::vec4 tetrahedronPlane = {1,1,1,-0.1f*tetrahedronSize};
	for (int i = 0; i < siteCount; ++i) {
		glm::vec4 point = {rand(siteRange), rand(siteRange), rand(siteRange), 1};
		if (glm::dot(point,tetrahedronPlane) < -delta)
			sites.emplace_back(point);
		else i--;
	}
	

	int initialInfo[] = {
		siteCount,               //0
		estimatedEdgeCount,      //1
		estimatedVertexCount,    //2
		estimatedIndexCount,     //3
		0,                       //4
		0,                       //5
		estimatedAuxIntCount,    //6
		estimatedAuxVecCount,    //7
		cutVerticesCount,        //8
		startingPointsCount,     //9
		usedIndicesCount,        //10
		usedVerticesCount,       //11
		vertexMappingCount,      //12
		auxVerticesUseCount,     //13
		auxVerticesCount,        //14
		vertexDistancesCount,    //15
		0,                       //16
		0,                       //17
		0,                       //18
		0,                       //19
		0,                       //20
		0,                       //21
		tetrahedronSize,         //22
		};
		
		
	//------------------------------------------------------------------
	//push known data into buffers and initialize them
	//------------------------------------------------------------------



	infoBuffer = new Buffer(GL_DYNAMIC_READ);
	infoBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	infoBuffer->createMutable(sizeof(initialInfo)*4, initialInfo);
	
	siteBuffer = new Buffer(GL_DYNAMIC_READ);
	siteBuffer->bind(GL_SHADER_STORAGE_BUFFER, 1);
	siteBuffer->createMutable(siteCount * sizeof(glm::vec4), sites.data());
	
	vertexBuffer = new Buffer(GL_DYNAMIC_READ);
	vertexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 2);
	vertexBuffer->allocateMutable(verticesByteSize);
	
	edgeBuffer = new Buffer(GL_DYNAMIC_READ);
	edgeBuffer->bind(GL_SHADER_STORAGE_BUFFER, 3);
	edgeBuffer->allocateMutable(edgesByteSize);
	
	siteDistancesBuffer = new Buffer(GL_DYNAMIC_READ);
	siteDistancesBuffer->bind(GL_SHADER_STORAGE_BUFFER, 4);
	siteDistancesBuffer->allocateMutable(siteDistancesByteSize);
	
	indexBuffer = new Buffer(GL_DYNAMIC_READ);
	indexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 5);
	indexBuffer->allocateMutable(indicesByteSize);
	
	commandBuffer = new Buffer(GL_DYNAMIC_READ);
	commandBuffer->bind(GL_SHADER_STORAGE_BUFFER, 6);
	commandBuffer->allocateMutable(commandsByteSize);
	
	vertexDistancesBuffer = new Buffer(GL_DYNAMIC_READ);
	vertexDistancesBuffer->bind(GL_SHADER_STORAGE_BUFFER, 7);
	vertexDistancesBuffer->allocateMutable(vertexDistancesByteSize);
	
	auxVecBuffer = new Buffer(GL_DYNAMIC_READ);
	auxVecBuffer->bind(GL_SHADER_STORAGE_BUFFER, 8);
	auxVecBuffer->allocateMutable(auxVecsByteSize);
	
	auxIntBuffer = new Buffer(GL_DYNAMIC_READ);
	auxIntBuffer->bind(GL_SHADER_STORAGE_BUFFER, 9);
	auxIntBuffer->allocateMutable(auxIntsByteSize);
	
	
	
	test0Buffer = new Buffer(GL_DYNAMIC_READ);
	test0Buffer->bind(GL_SHADER_STORAGE_BUFFER, 19);
	test0Buffer->allocateMutable(siteCount * siteCount * sizeof(glm::vec4));
	
	


	//------------------------------------------------------------------
	//compile shader and run it
	//------------------------------------------------------------------



	std::string sortAndCutShaderStr = sortAndCutHeaderSrc;
	sortAndCutShaderStr += sortAndCutSrc;
	
	sortAndCutShader = new Shader(GL_COMPUTE_SHADER, sortAndCutShaderStr);
	
	vstProgram = new Program();
	vstProgram->attachShader(sortAndCutShader);
	vstProgram->useProgram();
	
//	GLuint tQuery;
//	GLuint64 t;
//	glGenQueries(1, &tQuery);
	
	glFinish();
//	glBeginQuery(GL_TIME_ELAPSED, tQuery);
	auto t0 = std::chrono::steady_clock::now();
	
//	glDispatchCompute(1, 1, 1);
	glDispatchCompute(groupCount, 1, 1);
	
	glFinish();
//	glEndQuery(GL_TIME_ELAPSED);
	auto t1 = std::chrono::steady_clock::now();
//	glGetQueryObjectui64v(tQuery, GL_QUERY_RESULT, &t);
	const float time1 = (float)(std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count()) / 1000000.0f;


	//------------------------------------------------------------------
	//collect data and print
	//------------------------------------------------------------------



	printf("siteCount: %d\n", siteCount);
//	printf("OpenGL query time in ms: %f\n", (float)t / 1000000.0f);
	printf("real time in ms: %f\n", time1);
	
	vstProgram->detachShader(sortAndCutShader);
	
	
	int *intData = new int[1000];

	printf("tetrahedron scale: %f\n", (float)tetrahedronSize * 0.1f);
	infoBuffer->getData(intData, sizeof(initialInfo)*4);
	printf("max edges: %d\n", intData[16]);
	printf("max vertices: %d\n", intData[20]);
	printf("average cuts: %f\n", (float)intData[17]/siteCount);
//	printf("average cuts adjusted: %f\n", (float)(intData[17]-intData[19])/(siteCount-intData[19]));
	printf("amount of cells using all neighbours: %d\n", intData[18]);
//	printf("amount of 1 cuts: %d\n", intData[19]);
	

	delete[] intData;
	
	
	//------------------------------------------------------------------
	//set up render
	//------------------------------------------------------------------

	
	//setup render data and other settings
	Main::windowVAO->bind();
	
	vertexBuffer->bind(GL_ARRAY_BUFFER);
	indexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
	siteBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	
	Main::windowVAO->addAttribute(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);
	
	renderSize = siteCount;
	
	//setup render calls for callback
	Main::window->addRenderFunction([&]() -> int {
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		Main::windowVoronoiProgram->useProgram();
		Main::windowVoronoiProgram->uniformMat4("uVP", Main::windowOrbitManipulator->matrix);
		Main::windowVoronoiProgram->uniformFloat("uCellDistance", cellDistance);
		
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, renderSize, 0);
		
		return 0;
	});
	


	//------------------------------------------------------------------
	//set up update function and input function
	//------------------------------------------------------------------



/*	Main::window->addUpdateFunction([&](float t) -> int {
//		static float scale = 2;
//		static float timepulsing = 0;
//		Main::windowOrbitManipulator->rotate(t*200.0f,0);
//		cellDistance = scale + (sin(timepulsing+=t) * scale);
		cellDistance += cellDistance*t*2.0f;
		if (cellDistance > 50) cellDistance = 0;
		return 0;
	});*/
	
	//setup interactive input callback
	Main::window->addInputFunction([&](SDL_Event &e) -> int {
		int size = 0;
		SDL_Keycode command = e.key.keysym.sym;
		
		switch (e.type) {
		case SDL_KEYUP:
			if (command == SDLK_RIGHT) {
				
			} else if (command == SDLK_LEFT) {
				
			}
			break;
		case SDL_KEYDOWN:
			if (command == SDLK_RIGHT) {
				if (renderSize < 64) renderSize++;
			} else if (command == SDLK_LEFT) {
				if (renderSize > 0) renderSize--;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = false;
			}
			break;
		case SDL_MOUSEMOTION:
			if (rightMouseDown) {
				cellDistance += e.motion.xrel * sensitivity;
				cellDistance = cellDistance < 0.0f ? 0.0f : cellDistance;
			}
			break;
		default:
			break;
		}
		return 0;
	});

}


}} //end namespace VST::GPU
