//autor: Michael Ončo
#pragma once

#include <glwindow.h>

#include <buffer.h>
#include <program.h>
#include <shader.h>
#include <vertexarray.h>
#include <orbitmanipulator.h>


namespace Main {

extern bool windowInitialized;
extern OGL::GLWindow *window;

extern OGL::Program *windowWireProgram;
extern OGL::Program *windowPointProgram;
extern OGL::Program *windowVoronoiProgram;
extern OGL::Program *windowProgram;

extern OGL::Shader *windowVShader;
extern OGL::Shader *windowGShader;
extern OGL::Shader *windowFShader;
extern OGL::Shader *windowVoronoiVShader;
extern OGL::Shader *windowVoronoiGShader;
extern OGL::Shader *windowVoronoiFShader;
extern OGL::Shader *windowWireFShader;
extern OGL::Shader *windowPointFShader;

extern OGL::Buffer *windowVertexBuffer;
extern OGL::Buffer *windowIndexBuffer;

extern OGL::Buffer *windowCommandBuffer;
extern OGL::Buffer *windowVoronoiOffsetBuffer;

extern OGL::VertexArray *windowVAO;

extern OGL::OrbitManipulator *windowOrbitManipulator;




void initializeWindow();
void initializeWindowRender();
void runWindow();
void destroyWindow();

} // end namespace Main

