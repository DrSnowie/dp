//autor: Michael Ončo
#include <vst_cpu.h>
#include <vst_gpu.h>
#ifdef GFLIP
#include <gflip_addon.h>
#endif
#include <mainwindow.h>

#include <vst_gpu_init1.h>

int main(int argc, char *argv[]) {
	int implementation = 0;
	int groupCount = 1;
	int siteCount = 10;
	
	if (argc == 4) {
		implementation = atoi(argv[1]);
		groupCount = atoi(argv[2]);
		siteCount = atoi(argv[3]);
	} else if (argc == 1) { 
		printf("read the README to learn about arguments\n");
	} else {
		printf("invalid amount of arguments: %d\n", argc-1);
	}
	
	Main::initializeWindow();
	
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(OGL::MessageCallback, 0);
	
	switch(implementation) {
	case 0:
		printf("selected VST GPU implementation\n");
		VST::GPU::runAndDisplay(groupCount, siteCount);
		break;
#ifdef GFLIP
	case 1:
		printf("selected GFlipAddon implementation\n");
		GFlipAddon::runAndDisplay(groupCount, siteCount);
		break;
#else
	case 1:
		printf("selected GFlipAddon implementation\n");
		printf("you have to build the implementation first for this to work, look into the CMake GUI or README\n");
#endif
	case 2:
		printf("selected VST CPU implementation\n");
		VST::CPU::runAndDisplay(siteCount);
//		VST::CPU::step(siteCount);
		break;
	case 3:
		printf("selected the other VST GPU implementation -> needs more work and fixing\n");		
//		VST::GPU::initVer1(groupCount, siteCount);
//		Main::runWindow();
//		Main::destroyWindow();
//		VST::GPU::destroyVer1();
		break;
	default:
		printf("selected unknown implementation %d\n", implementation);
	}

	return 0;
}


















