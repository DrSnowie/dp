#include <vst_gpu.h>
#include <vst_gpu_init2.h>

#include <mainwindow.h>

using namespace OGL;

namespace VST {
namespace GPU {

void runAndDisplay(int groupCount, int siteCount) {
		initVer2(groupCount, siteCount);
		Main::runWindow();
		Main::destroyWindow();
		destroyVer2();
}

}} //end namespace VST::GPU
