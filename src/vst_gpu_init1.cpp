#include <vst_gpu.h>


#include <buffer.h>
#include <shader.h>
#include <program.h>

#include <vst_gpu_sortsites.h>
#include <vst_gpu_cutcells.h>

#include <mainwindow.h>

#include <vector>
//#include <array>
//#include <chrono>
#include <glm/glm.hpp>

#include <GL/glew.h>

using namespace OGL;

namespace VST {
namespace GPU {

static Buffer *infoBuffer = nullptr;
static Buffer *siteBuffer = nullptr;
static Buffer *vertexBuffer = nullptr;
static Buffer *edgeBuffer = nullptr;
static Buffer *sortSiteBuffer = nullptr;
static Buffer *indexBuffer = nullptr;
static Buffer *commandBuffer = nullptr;
static Buffer *offsetBuffer = nullptr;

static Buffer *sortingAuxBuffer = nullptr;

static Shader *sortingShader = nullptr;
static Shader *cuttingShader = nullptr;
static Shader *sortAndCutShader = nullptr;

static Program *vstProgram = nullptr;


static int siteCount = 10;
static float siteRange = 10.0f;

static float delta = 0.00001f;

static int renderSize = 0;
static float cellDistance = 0.0f;

static float sensitivity = 0.01f;
static bool rightMouseDown = false;

static int estimatedVertexCount = 1000;
static int estimatedEdgeCount = 1000;
static int estimatedIndexCount = 1000;


static const GLchar vstHeaderSrc[] = { R".(
#version 430

layout(binding=0)buffer Info{int info[];};
layout(binding=1)buffer Sites{vec4 sites[];};
layout(binding=2)buffer Vertices{vec4 vertices[];};
layout(binding=3)buffer Edges{ivec4 edges[];};
layout(binding=4)buffer SortedSites{vec4 sortedSites[];};
layout(binding=5)buffer Indices{int indices[];};
layout(binding=6)buffer Commands{int commands[];};
layout(binding=7)buffer Offsets{vec4 offsets[];};
layout(binding=8)buffer CuttingIndex{vec4 cuttingIndex[];};
layout(binding=9)buffer SortingAuxBuffer{vec4 sortingAuxBuffer[];};


const int siteCountIndex = 0;
const int vertexCountIndex = 1;
const int estimatedEdgesIndex = 2;
const int estimatedVerticesIndex = 3;
const int estimatedIndicesIndex = 4;
const int Index = 5;
const int oldEstimatedEdgesIndex = 6;
const int oldEstimatedVerticesIndex = 7;

)." };


void initVer1(int groupCount, int siteCount) {
	printf("VST GPU 2 kernel implementation.\nPress ENTER to start.\n");
	scanf("%*c");
	printf("init\n");
	
	
	std::vector<glm::vec4> sites;
	std::vector<glm::vec4> vertices;
	
	//std::vector<std::array<int, 200 * 4>> edges;
	
	
	glm::vec4 initialVertices[] = { { 0,0,0,1 },{ 30,0,0,1 },{ 0,30,0,1 },{ 0,0,30,1 } };
	//glm::ivec4 initialEdges[] = { { 0,1,0,1 },{ 0,2,0,2 },{ 0,3,1,2 },{ 1,2,0,3 },{ 1,3,1,3 },{ 2,3,2,3 } };
	//std::array<int, 200 * 4> initialEdges = {  0,1,0,1 , 0,2,0,2 , 0,3,1,2 , 1,2,0,3 , 1,3,1,3 , 2,3,2,3 , 0,0,-1,-1  };
	
	
	sites.reserve(siteCount);
	vertices.reserve(1000);
//	edges.reserve(siteCount);
	
	//std::srand(std::time(nullptr));
	
	vertices.push_back(initialVertices[0]);
	vertices.push_back(initialVertices[1]);
	vertices.push_back(initialVertices[2]);
	vertices.push_back(initialVertices[3]);
	
	float (*rand)(float) = [](float limit) -> float {
		return std::rand() / float(RAND_MAX) * limit;
	};
	
	for (int i = 0; i < siteCount; ++i) {
		sites.emplace_back(rand(10), rand(10), rand(10), 1);
//		edges.emplace_back(initialEdges); //5?
		//edges[i].reserve(100);
	}
	
	
	int initialInfo[] = {siteCount,-1,estimatedEdgeCount,estimatedVertexCount,estimatedIndexCount,-1};
	infoBuffer = new Buffer(GL_DYNAMIC_READ);
	infoBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	infoBuffer->createMutable(sizeof(initialInfo), initialInfo);
	
	siteBuffer = new Buffer(GL_DYNAMIC_READ);
	siteBuffer->bind(GL_SHADER_STORAGE_BUFFER, 1);
	siteBuffer->createMutable(sites.size() * sizeof(glm::vec4), sites.data());
//	siteBuffer->allocateMutable(vertexCount * 4 * sizeof(float));
//	siteBuffer->setData((void *)testVertices, vertexCount * 4 * sizeof(float));
	
	vertexBuffer = new Buffer(GL_DYNAMIC_READ);
	vertexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 2);
	vertexBuffer->allocateMutable(siteCount * estimatedVertexCount * sizeof(glm::vec4));
//	printf("\n\n\n%d\n\n\n", siteCount * estimatedVertexCount * sizeof(glm::vec4));
	vertexBuffer->setData(vertices.data(), vertices.size() * sizeof(glm::vec4));
//	printf("\n\n\n%d\n\n\n\n", siteCount * estimatedVertexCount * sizeof(glm::vec4));
	//nemel by byt problem nastavit
	
	edgeBuffer = new Buffer(GL_DYNAMIC_READ);
	edgeBuffer->bind(GL_SHADER_STORAGE_BUFFER, 3);
	edgeBuffer->allocateMutable(estimatedEdgeCount * sizeof(int) * 4 * siteCount);
//	edgeBuffer->setData(edges.data(), edges.size() * 200 * 4 * sizeof(int));
	//zde nastavit proste jen ve smycce nastavovat puvodni data s rozestupem... melo by to snad jit v pohode...
	
	sortSiteBuffer = new Buffer(GL_DYNAMIC_READ);
	sortSiteBuffer->bind(GL_SHADER_STORAGE_BUFFER, 4);
	sortSiteBuffer->allocateMutable(sites.size() * sites.size() * sizeof(glm::vec4));
	
	indexBuffer = new Buffer(GL_DYNAMIC_READ);
	indexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 5);
	indexBuffer->allocateMutable(estimatedIndexCount * sizeof(int) * siteCount);
	
	commandBuffer = new Buffer(GL_DYNAMIC_READ);
	commandBuffer->bind(GL_SHADER_STORAGE_BUFFER, 6);
	commandBuffer->allocateMutable(sites.size() * 5 * sizeof(int));
	
	offsetBuffer = new Buffer(GL_DYNAMIC_READ);
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 7);
	offsetBuffer->allocateMutable(sites.size() * sizeof(glm::vec4));
	
	sortingAuxBuffer = new Buffer(GL_DYNAMIC_READ);
	sortingAuxBuffer->bind(GL_SHADER_STORAGE_BUFFER, 9);
	sortingAuxBuffer->allocateMutable(sites.size() * sites.size() * sizeof(glm::vec4));
	
//	vertexInfoBuffer = new Buffer(GL_DYNAMIC_READ);
//	vertexInfoBuffer->bind(GL_SHADER_STORAGE_BUFFER, 4);
	
	//asi by nebylo od veci mit nejaky zakladni buffer ktery by obsahoval data na zkopirovani do techto struktur a pak mit shader na inicializaci ktery by to prekopiroval
	
	
	
	
	std::string sortingShaderStr = vstHeaderSrc;
	//sortingShaderStr += placeholderSortSitesSrc;
	sortingShaderStr += radixSortSitesSrc;
	
	std::string cuttingShaderStr = vstHeaderSrc;
	//cuttingShaderStr += placeholderCommandLoaderSrc;
	cuttingShaderStr += cutCellsSrc;
	
	vstProgram = new Program();
	
	sortingShader = new Shader(GL_COMPUTE_SHADER, sortingShaderStr);
	cuttingShader = new Shader(GL_COMPUTE_SHADER, cuttingShaderStr);
	
	
	vstProgram->attachShader(sortingShader);
	vstProgram->useProgram();
	
	GLuint tQuery;
	GLuint64 t;
	glGenQueries(1, &tQuery);
	
	glFinish();
	glBeginQuery(GL_TIME_ELAPSED, tQuery);
	
	glDispatchCompute(1, 1, 1);
	vstProgram->detachShader(sortingShader);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	
	/*
	float floatData1[10000];
	float floatData2[10000];
	
	const int sortedSize = siteCount * siteCount;
	sortSiteBuffer->getData(floatData1, sortedSize * sizeof(glm::vec4));
	siteBuffer->getData(floatData2, siteCount * sizeof(glm::vec4));
	
	printf("unsortedSitesTest:\n");
//	const int size = siteCount * 4;
	int j = 0;
	for (int i = 0; i < siteCount; ++i) {
		printf("%d: %f : %f : %f\n", i, floatData2[j+0], floatData2[j+1], floatData2[j+2]);
		j += 4;
	}
	
	printf("sortedSitesTest:\n");
//	const int size = siteCount * 4;
	j = 0;
	for (int i = 0; i < siteCount; ++i) {
		printf("%d=====================================\n", i);
		for (int k = 0; k < siteCount; ++k) {
			printf("%d:%d: %f : %f : %f = %f (", k, j, floatData1[j+0], floatData1[j+1], floatData1[j+2], floatData1[j+3]);
			union { int a; float b; } test;
			test.b = (floatData1[j + 3]);
			for (int i = 31; i >= 0; --i) printf("%d", (test.a >> i) & 1);
			printf(")\n");
			j += 4;
		}
	}
	
	sortingAuxBuffer->getData(floatData1, sortedSize * sizeof(glm::vec4));
	printf("auxSortBufferTest:\n");
	//	const int size = siteCount * 4;
	j = 0;
	for (int i = 0; i < siteCount; ++i) {
		printf("%d=====================================\n", i);
		for (int k = 0; k < siteCount; ++k) {
			printf("%d:%d: %f : %f : %f = %f (", k, j, floatData1[j + 0], floatData1[j + 1], floatData1[j + 2], floatData1[j + 3]);
			union { int a; float b; } test;
			test.b = (floatData1[j + 3]);
			for (int i = 31; i >= 0; --i) printf("%d", (test.a >> i) & 1);
			printf(")\n");
			j += 4;
		}
	}
	*/
	
	
	vstProgram->attachShader(cuttingShader);
	vstProgram->useProgram();
	glDispatchCompute(1, 1, 1);
	
	glFinish();
	glEndQuery(GL_TIME_ELAPSED);
	glGetQueryObjectui64v(tQuery, GL_QUERY_RESULT, &t);
	printf("siteCount: %d\n", siteCount);
	printf("OpenGL query time in ms: %f\n", (float)t / 1000000.0f);
	
	vstProgram->detachShader(cuttingShader);
//	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	
	/*
	float floatData[100000];
	int intData[100000];
	
//	siteCount = 1;
	
//	printf("\n\n\n%d\n\n\n\n", siteCount * estimatedVertexCount * sizeof(glm::vec4));
	vertexBuffer->getData(floatData, siteCount * estimatedVertexCount * sizeof(glm::vec4));
//	vertexBuffer->getData(floatData, sizeof(glm::vec4));
	printf("vertexBuffer:\n");
//	const int size = siteCount * 4;
	int j = 0;
//	for (int k = 0; k < siteCount; ++k) {
	for (int k = 0; k < 2; ++k) {
		j = k * estimatedVertexCount*4;
		for (int i = 0; i < /*estimatedVertexCount*//*20; ++i) {
			printf("%d:%d: %f : %f : %f\n", k, i, floatData[j + 0], floatData[j + 1], floatData[j + 2]);
			j += 4;
		}
	}
	
	indexBuffer->getData(intData, siteCount * estimatedIndexCount * sizeof(int));
	printf("indexBuffer:\n");
//	const int size = siteCount * 4;
	j = 0;
	for (int k = 0; k < 2; ++k) {
		j = k * estimatedIndexCount;
		for (int i = 0; i < /*estimatedVertexCount*//*20; ++i) {
			printf("%d:%d: %d : %d : %d\n", k, i, intData[j + 0], intData[j + 1], intData[j + 2]);
			j += 3;
		}
	}
	
	commandBuffer->getData(intData, siteCount * 5 * sizeof(int));
	printf("commandBuffer:\n");
//	const int size = siteCount * 4;
	j = 0;
	for (int i = 0; i < siteCount; ++i) {
		printf("%d: %d : %d : %d : %d : %d\n", i, intData[j+0], intData[j+1], intData[j+2], intData[j+3], intData[j+4]);
		j += 5;
	}
	
	offsetBuffer->getData(floatData, siteCount * sizeof(glm::vec4));
	printf("offsetBuffer:\n");
//	const int size = siteCount * 4;
	j = 0;
	for (int i = 0; i < siteCount; ++i) {
		printf("%d: %f : %f : %f\n", i, floatData[j+0], floatData[j+1], floatData[j+2]);
		j += 4;
	}
	
	edgeBuffer->getData(intData, siteCount * sizeof(glm::ivec4));
	
	printf("edgeBuffer:\n");
//	const int size = siteCount * 4;
	j = 0;
//	for (int k = 0; k < siteCount; ++k) {
	for (int k = 0; k < 2; ++k) {
		j = k * estimatedEdgeCount;//*4;
		for (int i = 0; i < /*estimatedVertexCount*//*20; ++i) {
			printf("%d:%d: %d : %d : %d : %d\n", k, i, intData[j+0], intData[j+1], intData[j+2], intData[j+3]);
			j += 4;
		}
	}
	
	
	infoBuffer->getData(intData, 6 * sizeof(int));
	printf("infoBuffer:\n");
	for (int i = 0; i < 6; ++i) {
		printf("%d: %d\n", i, intData[i]);
	}
	
	
	*/
	
	vertexBuffer->bind(GL_ARRAY_BUFFER);
	indexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	renderSize = siteCount;
//	renderSize = 1;
	
		
	Main::windowVAO->bind();
	Main::windowVAO->addAttribute(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	
	
	Main::window->addRenderFunction([&]() -> int {
		
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		Main::windowVoronoiProgram->useProgram();
		Main::windowVoronoiProgram->uniformMat4("uVP", Main::windowOrbitManipulator->matrix);
		Main::windowVoronoiProgram->uniformFloat("uCellDistance", cellDistance);
		Main::windowVAO->bind();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		//		Buffer::unbind(GL_ARRAY_BUFFER);
		//		Buffer::unbind(GL_ELEMENT_ARRAY_BUFFER);
		
		vertexBuffer->bind(GL_ARRAY_BUFFER);
		indexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
		commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
		offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER);
		
//		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, 1, 0);
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, renderSize, 0);
		//		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (void *)(0 * 5 * sizeof(int)), 1, 0);
		
		return 0;
	});
	
	
	Main::window->addInputFunction([&](SDL_Event &e) -> int {
//		int intData[bufferSize];
//		float floatData[bufferSize];
		int size = 0;
		//		while (true) {
		SDL_Keycode command = e.key.keysym.sym;
		//			std::cin >> command;
		/*		if (e.key.state == SDL_PRESSED) {
		if (command == SDLK_r) {
		std::cout << "-------------------------------------\n             TEST\n\n";
		//				runAlgorithmRound();
		//				freeBuffer->getData(&tetrahedronCount, sizeof(int));
		//				setUpIndicesForRender();
		} else if (command == SDLK_1) {
		} else if (command == SDLK_UP) {
		}
		}	*/
		
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = false;
			}
			break;
		case SDL_MOUSEMOTION:
			if (rightMouseDown) {
				cellDistance += e.motion.xrel * sensitivity;
				cellDistance = cellDistance < 0.0f ? 0.0f : cellDistance;
				//this->rotate(e.motion.xrel, e.motion.yrel);
				//				MOTION(e.motion.xrel, e.motion.yrel);
			}
			break;
		default:
			break;
		}
		return 0;
	});
	
}

void destroyVer1() {
	delete siteBuffer;
	delete vertexBuffer;
	delete edgeBuffer;
	delete sortSiteBuffer;
	delete indexBuffer;
	delete commandBuffer;
	delete offsetBuffer;
	delete sortingAuxBuffer;
	//delete vertexInfoBuffer;
}

}} //end namespace VST::GPU
