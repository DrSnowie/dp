#pragma once

#include <GL/glew.h>

namespace VST {
namespace GPU {

extern const GLchar placeholderCommandLoaderSrc[];

extern const GLchar cutCellsSrc[];

}} //end namespace VST::GPU

