#pragma once

#include <GL/glew.h>

namespace VST {
namespace GPU {

extern const GLchar placeholderSortSitesSrc[];

extern const GLchar radixSortSitesSrc[];

}} //end namespace VST::GPU

