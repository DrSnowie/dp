//autor: Michael Ončo
#include <vst_cpu.h>

#include <vst_cpu_sitesortinginfo.h>
#include <vst_cpu_cellvertexinfo.h>

#include <mainwindow.h>


#include <vector>
#include <glm/glm.hpp>

#include <buffer.h>

#include <chrono>

using namespace OGL;

namespace VST {
namespace CPU {


static Buffer *verticesBuffer = nullptr;
static Buffer *indicesBuffer = nullptr;
static Buffer *commandBuffer = nullptr;
static Buffer *offsetBuffer = nullptr;


static int renderSize = 0;
static float cellDistance = 0.0f;

static float sensitivity = 0.01f;
static bool rightMouseDown = false;


//static int siteCount = 64;
static const float siteRange = 10.0f;

static const float delta = 0.00001f;

static std::vector<glm::vec4> sites;
static std::vector<glm::vec4> vertices;
static std::vector<std::vector<glm::ivec4>> edges;
static std::vector<glm::vec4> planes;

static struct SiteSortingInfo sortedSites;
static struct CellVerticesInfo cellVerticesInfo;



//------------------------------------------------------------------
//helper functions
//------------------------------------------------------------------



float findFurthestCellPoint(struct CellVerticesInfo &cellVertexDistances) {
	const float distance = cellVertexDistances.simpleFindFurthest();
	return distance;
}





float findClosestSite(glm::vec4 &closestSite, const std::vector<glm::vec4> &sites, struct SiteSortingInfo &sortedSites) {
	float siteDistance = sortedSites.simpleFindClosest(closestSite);
	return siteDistance;
}





static void addCutVertex(const int vertexIndex, const int polygonIndex, std::vector<glm::ivec2> &cutVertices) {
	int size = cutVertices.size();
	if (size <= polygonIndex) {
		cutVertices.resize(polygonIndex + 1);
		int newSize = cutVertices.size();
		for (int i = size; i < newSize; ++i) {
			cutVertices[i] = glm::ivec2(-1, -1);
		}
	};
	if (cutVertices[polygonIndex][0] == -1) cutVertices[polygonIndex][0] = vertexIndex;
	else if (cutVertices[polygonIndex][1] == -1) cutVertices[polygonIndex][1] = vertexIndex;
}





static bool eq(const glm::vec3 &a, const glm::vec3 &b, const float delta) {
	return a.x > b.x - delta && a.x < b.x + delta
		&& a.y > b.y - delta && a.y < b.y + delta
		&& a.z > b.z - delta && a.z < b.z + delta;
}





static bool eq(const glm::vec4 &a, const glm::vec4 &b, const float delta) {
	return a.x > b.x - delta && a.x < b.x + delta
		&& a.y > b.y - delta && a.y < b.y + delta
		&& a.z > b.z - delta && a.z < b.z + delta
		&& a.w > b.w - delta && a.w < b.w + delta;
}





static int addVertex(const glm::vec4 &newVertex, std::vector<glm::vec4> &vertices) {
	int newIndex = -1;
	int size = vertices.size();
	for (int l = 0; l < size; l += 1) {
		const glm::vec4 point = vertices[l];
		if (eq(point, newVertex, delta)) {
			newIndex = l;
			break;
		}
	}
	
	if (newIndex == -1) {
		newIndex = vertices.size();
		vertices.push_back(newVertex);
	}
	return newIndex;
}





void cutEdge
(int inIndex
,int outIndex
,glm::ivec4 &edge
,const glm::vec4 &cuttingPlane
,std::vector<glm::ivec2> &cutVertices
,std::vector<glm::vec4> &vertices
,std::vector<glm::ivec4> &edges
,struct CellVerticesInfo &cellVertexDistances) {
	const glm::vec4 inVertex = vertices[edge[inIndex]];
	const glm::vec4 outVertex = vertices[edge[outIndex]];
	const glm::vec4 dir = inVertex - outVertex;
	const float aux1 = glm::dot(-cuttingPlane, inVertex);
	const float aux2 = glm::dot(cuttingPlane, dir);
	const float position = aux1 / aux2;
	const glm::vec4 newVertex = (position * dir) + inVertex;

	const int newVIndex = addVertex(newVertex, vertices);
	addCutVertex(newVIndex, edge[2], cutVertices);
	addCutVertex(newVIndex, edge[3], cutVertices);


	const int oldVIndex = edge[outIndex];
	edge[outIndex] = newVIndex;


	if (newVIndex != oldVIndex) {
		cellVertexDistances.add(newVIndex);
		cellVertexDistances.remove(oldVIndex);
	} else {
		cellVertexDistances.remove(oldVIndex);
		cellVertexDistances.add(newVIndex);
	}
}





void cutEdges
(const glm::vec3 &site1
,const glm::vec3 &site2
,std::vector<glm::vec4> &vertices
,std::vector<glm::ivec4> &edges
,struct CellVerticesInfo &cellVertexDistances
,int polygonIndex) {
	std::vector<glm::ivec2> cutVertices(50, {-1,-1});
	
	glm::vec3 planePoint = (site2 + site1) * 0.5f;
	glm::vec3 planeNormal = glm::normalize(site2 - site1);
	float planeConst = -(glm::dot(planePoint, planeNormal));
	glm::vec4 plane = { planeNormal, planeConst };
	
	int edgeCount = edges.size();
	for (int i = 0; i < edgeCount; ++i) {
		glm::ivec4 &edge = edges[i];
		if (edge[0] == edge[1]) continue;
		glm::vec4 linePoints[2] = { vertices[edge[0]], vertices[edge[1]] };
		
		char out = 0;
		char eq = 0;
		const float pointOrientation[2] = {
			glm::dot(plane, linePoints[0]),
			glm::dot(plane, linePoints[1])
		};
		
		out |= (pointOrientation[0] > delta) << 0;
		out |= (pointOrientation[1] > delta) << 1;
		
		switch (out) {
		case 0:
			break;
		case 1:
			cutEdge(1, 0, edge, plane, cutVertices, vertices, edges, cellVertexDistances);
			break;
		case 2:
			cutEdge(0, 1, edge, plane, cutVertices, vertices, edges, cellVertexDistances);
			break;
		case 3:
			cellVertexDistances.remove(edge[0]);
			cellVertexDistances.remove(edge[1]);
			edge = glm::ivec4(0, 0, -1, -1);
			break;
		default:
			break;
		}
		
	}
	int size = cutVertices.size();
	int edgeIndex = edges.size();
	for (int i = 0; i < size; ++i) {
		if (cutVertices[i][1] != -1) {
			edges.emplace_back(cutVertices[i][0], cutVertices[i][1], i, polygonIndex);
		} else if (cutVertices[i][0] != -1) {
			const auto &v = vertices[cutVertices[i][0]];
		}
	}
	
}





void setUpDraw(std::vector<std::vector<glm::ivec4>> &edges, std::vector<glm::vec4> &vertices, std::vector<glm::vec4> &sites) {
	renderSize = sites.size();
	
	//------------------------------------------------------------------
	//initialize data and convert all polygons into triangles and commands
	//------------------------------------------------------------------

	std::vector<int> indicesData;
	indicesData.reserve(1000);
	std::vector<glm::ivec2> polygonStartingEdge(1000, {-1,-1});
	struct {int a; int b; int c; int d; int e;} command;
	std::vector<decltype(command)> commandData;
	commandData.reserve(1000);
	int indexCount = 0;
	int indexTotal = 0;
	int index = 0;
	for (auto &eBuffer : edges) {
		
		for (auto &i : polygonStartingEdge) { i = glm::ivec2(-1, -1); }
		indexCount = 0;
		for (auto &e : eBuffer) {
			if (e[0] == e[1]) continue;
			if (e[2] >= polygonStartingEdge.size() || e[3] >= polygonStartingEdge.size()) {
				const int oldSize = polygonStartingEdge.size();
				const int newSize = e[2] > e[3] ? e[2] : e[3];
				polygonStartingEdge.resize(newSize);
				for (int i = 0; i < newSize; ++i) {
					polygonStartingEdge[i] = glm::ivec2{-1, -1};
				}
			}
			
			glm::ivec2 &startingEdge1 = polygonStartingEdge[e[2]];
			glm::ivec2 &startingEdge2 = polygonStartingEdge[e[3]];
			if (startingEdge1[0] == -1) {
				startingEdge1 = glm::ivec2(e[0], e[1]);
			} else if (startingEdge1[0] != e.x && startingEdge1[0] != e.y) {
				indicesData.push_back(e[0]);
				indicesData.push_back(e[1]);
				indicesData.push_back(startingEdge1[0]);
				indexCount += 3; 
			}
			if (startingEdge2[0] == -1) {
				startingEdge2 = glm::ivec2(e[0], e[1]);
			} else if (startingEdge2[0] != e.x && startingEdge2[0] != e.y){
				indicesData.push_back(e[0]);
				indicesData.push_back(e[1]);
				indicesData.push_back(startingEdge2[0]);
				indexCount += 3;
			}
		}
		
		command = {indexCount, 1, indexTotal, 0, index};
		commandData.push_back(command);
		indexTotal += indexCount;
		index++;
	}
	
	//------------------------------------------------------------------
	//initialize buffers for render
	//------------------------------------------------------------------

	int intData[10000];
	float floatData[10000];
	int size = 0;
	
	Main::windowVAO->bind();
	
	verticesBuffer = new Buffer(GL_STATIC_DRAW);
	verticesBuffer->bind(GL_ARRAY_BUFFER);
	verticesBuffer->createMutable(vertices.size() * sizeof(glm::vec4), vertices.data());
	
	indicesBuffer = new Buffer(GL_STATIC_DRAW);
	indicesBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	indicesBuffer->createMutable(indexTotal * sizeof(int), indicesData.data());
	
	commandBuffer = new Buffer(GL_STATIC_DRAW);
	commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
	commandBuffer->createMutable(renderSize * sizeof(decltype(command)), commandData.data());
	
	offsetBuffer = new Buffer(GL_STATIC_DRAW);
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	offsetBuffer->createMutable(renderSize * 4 * sizeof(float), sites.data());
	


	//------------------------------------------------------------------
	//set up render and update functions
	//------------------------------------------------------------------



	Main::windowVAO->addAttribute(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	
	Main::windowVoronoiProgram->useProgram();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);
	
	Main::window->addRenderFunction([&]() -> int {
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		Main::windowVoronoiProgram->uniformMat4("uVP", Main::windowOrbitManipulator->matrix);
		Main::windowVoronoiProgram->uniformFloat("uCellDistance", cellDistance);
		
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, renderSize, 0);
		
		return 0;
	});
	
	
	Main::window->addInputFunction([&](SDL_Event &e) -> int {
		int size = 0;
		SDL_Keycode command = e.key.keysym.sym;
		
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = false;
			}
			break;
		case SDL_MOUSEMOTION:
			if (rightMouseDown) {
				cellDistance += e.motion.xrel * sensitivity;
				cellDistance = cellDistance < 0.0f ? 0.0f : cellDistance;
			}
			break;
		default:
			break;
		}
		return 0;
	});
	
}





void init(int siteCount) {
	glm::vec4 initialVertices[] = { { 0,0,0,1 },{ 30,0,0,1 },{ 0,30,0,1 },{ 0,0,30,1 } };
	glm::ivec4 initialEdges[] = { { 0,1,0,1 },{ 0,2,0,2 },{ 0,3,1,2 },{ 1,2,0,3 },{ 1,3,1,3 },{ 2,3,2,3 } };
	
	
	sites.reserve(siteCount);
	vertices.reserve(1000);
	edges.reserve(siteCount);
	
	vertices.push_back(initialVertices[0]);
	vertices.push_back(initialVertices[1]);
	vertices.push_back(initialVertices[2]);
	vertices.push_back(initialVertices[3]);
	
	float (*rand)(float) = [](float limit) -> float {
		return std::rand() / float(RAND_MAX) * limit;
	};
	
	for (int i = 0; i < siteCount; ++i) {
		sites.emplace_back(rand(10), rand(10), rand(10), 1);
		edges.emplace_back(initialEdges, initialEdges + 6); //5?
		edges[i].reserve(100);
	}

	//------------------------------------------------------------------
	//initialize data for voronoi diagram creation
	//------------------------------------------------------------------

	
	
	auto t0 = std::chrono::steady_clock::now();
	//cut while needed
	for (int i = 0; i < siteCount; ++i) {
		bool continueCutting = true;
		cellVerticesInfo.init(vertices, sites[i]);
		sortedSites.init(sites, i);
		int polygonIndex = 4;
		for (int j = 0; j < siteCount-1; ++j) {
			glm::vec4 closestSite;
			float closest = findClosestSite(closestSite, sites, sortedSites) / 4.0f;
			float furthest = findFurthestCellPoint(cellVerticesInfo);
			if (furthest == -1.0f || closest > furthest) break;
			continueCutting = closest < furthest;
			cutEdges(sites[i], closestSite, vertices, edges[i], cellVerticesInfo, polygonIndex);
			++polygonIndex;
		}
	}


	//------------------------------------------------------------------
	//finish VST and set up draw
	//------------------------------------------------------------------

	
	setUpDraw(edges, vertices, sites);
	glFinish();
	auto t1 = std::chrono::steady_clock::now();
	
	float t = (float)std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count() / 1000000.0f;
	printf("number of sites: %d\n", siteCount);
	printf("elapsed time from start of cutting to the end of upload to GPU: %f ms\n", t);
}





void destroy() {
	delete verticesBuffer;
	delete indicesBuffer;
	delete commandBuffer;
	delete offsetBuffer;
}





void run() {
	printf("run\n");
	Main::runWindow();
	destroy();
	Main::destroyWindow();
}




//------------------------------------------------------------------
//computes triangulation from polygons and fills buffers
//------------------------------------------------------------------


void setUpBuffers(std::vector<std::vector<glm::ivec4>> &edges, std::vector<glm::vec4> &vertices, std::vector<glm::vec4> &sites) {
	renderSize = 1;
	
	std::vector<int> indicesData;
	indicesData.reserve(1000);
	std::vector<glm::ivec2> polygonStartingEdge(1000, {-1,-1});
	struct {int a; int b; int c; int d; int e;} command;
	std::vector<decltype(command)> commandData;
	commandData.reserve(1000);
	int indexCount = 0;
	int indexTotal = 0;
	int index = 0;
	for (auto &eBuffer : edges) {
		
		for (auto &i : polygonStartingEdge) { i = glm::ivec2(-1, -1); }
		indexCount = 0;
		for (auto &e : eBuffer) {
			if (e[0] == e[1]) continue;
			if (e[2] >= polygonStartingEdge.size() || e[3] >= polygonStartingEdge.size()) {
				const int oldSize = polygonStartingEdge.size();
				const int newSize = e[2] > e[3] ? e[2] : e[3];
				polygonStartingEdge.resize(newSize);
				for (int i = 0; i < newSize; ++i) {
					polygonStartingEdge[i] = glm::ivec2{-1, -1};
				}
			}
			
			glm::ivec2 &startingEdge1 = polygonStartingEdge[e[2]];
			glm::ivec2 &startingEdge2 = polygonStartingEdge[e[3]];
			if (startingEdge1[0] == -1) {
				startingEdge1 = glm::ivec2(e[0], e[1]);
			} else if (startingEdge1[0] != e.x && startingEdge1[0] != e.y) {
				indicesData.push_back(e[0]);
				indicesData.push_back(e[1]);
				indicesData.push_back(startingEdge1[0]);
				indexCount += 3; 
			}
			if (startingEdge2[0] == -1) {
				startingEdge2 = glm::ivec2(e[0], e[1]);
			} else if (startingEdge2[0] != e.x && startingEdge2[0] != e.y){
				indicesData.push_back(e[0]);
				indicesData.push_back(e[1]);
				indicesData.push_back(startingEdge2[0]);
				indexCount += 3;
			}
		}
		
		command = {indexCount, 1, indexTotal, 0, index};
		commandData.push_back(command);
		indexTotal += indexCount;
		index++;
	}
	
	int intData[10000];
	float floatData[10000];
	int size = 0;
	
	Main::windowVAO->bind();
	
//	verticesBuffer = new Buffer(GL_STATIC_DRAW);
	verticesBuffer->bind(GL_ARRAY_BUFFER);
	verticesBuffer->createMutable(vertices.size() * sizeof(glm::vec4), vertices.data());
	
//	indicesBuffer = new Buffer(GL_STATIC_DRAW);
	indicesBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	indicesBuffer->createMutable(indexTotal * sizeof(int), indicesData.data());
	
//	commandBuffer = new Buffer(GL_STATIC_DRAW);
	commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
	commandBuffer->createMutable(renderSize * sizeof(decltype(command)), commandData.data());
	
//	offsetBuffer = new Buffer(GL_STATIC_DRAW);
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	offsetBuffer->createMutable(renderSize * 4 * sizeof(float), sites.data());
	
}



//------------------------------------------------------------------
//stepping through cutting single cell with mouse button presses
//------------------------------------------------------------------



void step(int siteCount) {
	glm::vec4 initialVertices[] = { { 0,0,0,1 },{ 30,0,0,1 },{ 0,30,0,1 },{ 0,0,30,1 } };
	glm::ivec4 initialEdges[] = { { 0,1,0,1 },{ 0,2,0,2 },{ 0,3,1,2 },{ 1,2,0,3 },{ 1,3,1,3 },{ 2,3,2,3 } };
	
	sites.reserve(siteCount);
	vertices.reserve(1000);
	edges.reserve(siteCount);
	
	vertices.push_back(initialVertices[0]);
	vertices.push_back(initialVertices[1]);
	vertices.push_back(initialVertices[2]);
	vertices.push_back(initialVertices[3]);
	
	float (*rand)(float) = [](float limit) -> float {
		return std::rand() / float(RAND_MAX) * limit;
	};
	
	for (int i = 0; i < siteCount; ++i) {
		sites.emplace_back(rand(10), rand(10), rand(10), 1);
		edges.emplace_back(initialEdges, initialEdges + 6); //5?
		edges[i].reserve(100);
	}
	
	setUpDraw(edges, vertices, sites);
	setUpBuffers(edges, vertices, sites);
	
	static int i = 0;
	static int j = 0;
	static bool continueCutting = true;
	cellVerticesInfo.init(vertices, sites[i]);
	sortedSites.init(sites, i);
	static int polygonIndex = 4;
	static bool buttonDown = false;

	Main::window->addInputFunction([&](SDL_Event &e) -> int {
		int size = 0;
		SDL_Keycode command = e.key.keysym.sym;
		
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = true;
			} else if (e.button.button == SDL_BUTTON_X2) {
//				printf("jsem tu1!!\n");
			
				//cut while needed
//				for (int i = 0; i < siteCount; ++i) {
//					for (int j = 0; j < siteCount-1; ++j) {
				if (continueCutting && j < siteCount-1 && !buttonDown) {
//					printf("jsem tu2!!\n");
					buttonDown = true;
					glm::vec4 closestSite;
					float closest = findClosestSite(closestSite, sites, sortedSites) / 4.0f;
					float furthest = findFurthestCellPoint(cellVerticesInfo);
					if (furthest == -1.0f || closest > furthest) break;
					continueCutting = closest < furthest;
					cutEdges(sites[i], closestSite, vertices, edges[i], cellVerticesInfo, polygonIndex);
					++polygonIndex;
					setUpBuffers(edges, vertices, sites);
				}
//					}
//				}
			
			
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = false;
			} else if (e.button.button == SDL_BUTTON_X2) {
				buttonDown = false;
			}
			break;
		default:
			break;
		}
		return 0;
	});

	Main::window->addUpdateFunction([&](float t) -> int {
		static float scale = 2;
		static float timepulsing = 0;
		Main::windowOrbitManipulator->rotate(t*200.0f,0);
//		cellDistance = scale + (sin(timepulsing+=t) * scale);
		return 0;
	});
	
	run();
}

void runAndDisplay(int siteCount) {
	init(siteCount);
	run();
}

}} //end namespace VST::CPU

