//autor: Michael Ončo
#pragma once

//#define SHADER(VA_ARGS) #VA_ARGS
//#define START_SHADER R".(
//#define END_SHADER ).";

namespace GFlipAddon {

//------------------------------------------------------------------
//own shaders from time this code was separate from main program
//------------------------------------------------------------------

static const char vSrc[] = R".(
#version 450

#extension GL_ARB_shader_draw_parameters : require

layout(location=0) in vec4 coord;

layout(binding=0)buffer Offsets{float offsets[];};

uniform mat4 uVP = mat4(1.0f);
uniform float uCellDistance = 0.0f;

out int vInstanceID;
out vec4 vSite;

void main() {
	vInstanceID = gl_BaseInstanceARB + gl_InstanceID;
	const int offsetOffset = vInstanceID * 3;
	vec4 offsetVector = vec4(offsets[offsetOffset+0], offsets[offsetOffset+1], offsets[offsetOffset+2], 0);
	vec4 site = vec4(offsetVector.xyz, 1);
//		vOffset = uVP * vec4(offsets[vInstanceID].xyz, 1);
//		gl_Position = uVP * (coord + offsets[vInstanceID] * uCellDistance);
	vSite = uVP * (site + offsetVector * uCellDistance);
	gl_Position = uVP * (coord + offsetVector * uCellDistance);
	gl_PointSize = gl_Position.z == 0 ? 0.0f : 100.0f / gl_Position.z;
}
).";

static const char gSrc[] = R".(
#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in int vInstanceID[];
in vec4 vSite[];

out int gInstanceID;
out vec4 gNormal;

void main() {
	gInstanceID = vInstanceID[0];

	vec3 v1 = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
	vec3 v2 = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
	vec4 normal = vec4(normalize(cross(v2, v1)), 0);

	//TODO make sure this works right
	if (dot(normal, normalize(gl_in[0].gl_Position - vSite[0])) > 0) normal = -normal;

	gl_Position = gl_in[0].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[1].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	gl_Position = gl_in[2].gl_Position;
	gNormal = normal;
	EmitVertex();
	
	EndPrimitive();
}
).";

static const char fSrc[] = R".(
#version 450

out vec4 fColor;

flat in int gInstanceID;
in vec4 gNormal;

uniform vec4 uDirection = vec4(0,0,1,0);
uniform vec4 uColor = vec4(0.1,1,1,1);
uniform float uAmbient = 0.1f;

void main() {
	vec4 color = uColor;
	vec4 direction = normalize(uDirection);
	float diffuse = dot(gNormal, direction);

	fColor = color * clamp((diffuse + uAmbient), 0, 1);
}
).";


//------------------------------------------------------------------
//convert DT to VD
//------------------------------------------------------------------


static const char setupSrc[] = R".(
#version 450

layout(binding=0)buffer Sites{float sites[];};
layout(binding=1)buffer Triangulation{ivec4 triangulation[];};
layout(binding=2)buffer Info{int info[];};
layout(binding=3)buffer Vertices{vec4 vertices[];};
layout(binding=4)buffer Indices{int indices[];};
layout(binding=5)buffer Commands{int commands[];};
layout(binding=6)buffer Edges{ivec4 edges[];};
layout(binding=7)buffer AuxVertices{vec4 auxVertices[];};
layout(binding=8)buffer Test1{int test1[];};
layout(binding=9)buffer Test2{vec4 test2[];};
//TODO use buffers for auxilliary arrays here for better size control
//TODO check if any thread accesses out of bounds - most likely with edges, auxVertices, usedIndices and so on

const int siteCountIndex = 0;
const int tetrahedronCountIndex = 1;
const int estimatedVerticesIndex = 2;
const int estimatedEdgesIndex = 3;
const int estimatedIndicesIndex = 4;
const int estimatedAuxVerticesIndex = 5;
const int vertexFreeSpaceIndex = 6;
const int indexFreeSpaceIndex = 7;

const int testIndex0 = 8;
const int testIndex1 = 9;
const int testIndex2 = 10;
const int testIndex3 = 11;
const int tetrahedronSizeIndex = 12;
const int testIndex4 = 13;


layout(local_size_x=32)in;


uniform float uDelta = 0.000001f;


void main() {


	//------------------------------------------------------------------
	//initialize data for whole thread
	//------------------------------------------------------------------



//	uint computeId = gl_LocalInvocationIndex;
	uint localId = gl_LocalInvocationID.x;
	uint globalId = gl_GlobalInvocationID.x;
	uint globalSize = gl_WorkGroupSize.x * gl_NumWorkGroups.x;
	
	const int siteCount = info[siteCountIndex];
	const int tetrahedronCount = info[tetrahedronCountIndex];



	//------------------------------------------------------------------
	//start main loop
	//------------------------------------------------------------------

	
//	for (int siteId = int(globalId); siteId < 2; siteId += int(globalSize)) {
//	for (int siteId = int(globalId); siteId < 100; siteId += int(globalSize)) {
	for (int siteId = int(globalId); siteId < siteCount; siteId += int(globalSize)) {
		
//		int cuttingIteration = 1;
		float furthestVertex = 9999.0f;
		int mySiteIndex = siteId * 3;
		vec3 mySite = vec3(sites[mySiteIndex+0], sites[mySiteIndex+1], sites[mySiteIndex+2]);
		vec4 auxVerticesTest[200];
		
		float tetrahedronSize = info[tetrahedronSizeIndex];
		const vec4 initialVertices[4] = {
			vec4(0,0,0,1),
			vec4(tetrahedronSize*0.1f,0,0,1),
			vec4(0,tetrahedronSize*0.1,0,1),
			vec4(0,0,tetrahedronSize*0.1,1),
		};
		
		
		const ivec4 initialEdges[6] = {
			ivec4(0,1,0,1),
			ivec4(0,2,0,2),
			ivec4(0,3,1,2),
			ivec4(1,2,0,3),
			ivec4(1,3,1,3),
			ivec4(2,3,2,3),
		};

		//------------------------------------------------------------------
		//initialize data for current site
		//------------------------------------------------------------------

		
		
//		const int verticesOffset = int(siteId) * info[estimatedVerticesIndex];
//		const int auxVerticesOffset = int(siteId) * info[estimatedAuxVerticesIndex];
//		const int edgesOffset = int(siteId) * info[estimatedEdgesIndex];
//		const int indicesOffset = int(siteId) * info[estimatedIndicesIndex];
//		const int sortedSitesOffset = int(siteId * siteCount);
		
		const int auxVerticesOffset = int(globalId) * info[estimatedAuxVerticesIndex];
		const int edgesOffset = int(globalId) * info[estimatedEdgesIndex];
		
		for (int i = 0; i < 4; ++i) {
			//TODO maybe make initialVertices modifiable or something
//			auxVertices[auxVerticesOffset+i] = initialVertices[i];
			auxVerticesTest[i] = initialVertices[i];
		}
		
		for (int i = 0; i < 6; ++i) {
			edges[edgesOffset+i] = initialEdges[i]; //TODO
//			edges[edgesOffset+i] = initialEdges[i]; //TODO
		}
		const int estimatedEdgesCount = info[estimatedEdgesIndex];
		for (int i = 6; i < estimatedEdgesCount; ++i) {
			//TODO for some reason this is necessary for future reuse of edges buffer - this shouldn't be necessary.. 
			//all data inside the area limited by edgeCount shouldn't contain invalid edges... and I shouldn't access anything outside
			//so either I'm trying to access outside of this area or I'm not adding edges in sequence... perheps I don't understand something here
			//maybe this is the cause of the problems???
			edges[edgesOffset+i] = ivec4(0,0,-1,-1);
		}
		
		
//		offsets[siteId] = vec4(sites[siteId].xyz, 0);
		int edgeCount = 6;
		int vertexCount = 4;
		int indexCount = 0;
		int faceCount = 4;
		
		int cutCount = 0;
		int cutSites[200];
		for (int i = 0; i < 200; ++i) cutSites[i] = 0;
		
		//------------------------------------------------------------------
		//convert the triangulation into easier to use format
		//------------------------------------------------------------------



		for (int triangulationIndex = 0; triangulationIndex < tetrahedronCount; ++triangulationIndex) {
//		for (int triangulationIndex = 0; triangulationIndex < 0; ++triangulationIndex) {
			ivec4 tetrahedron = triangulation[triangulationIndex];
			
			for (int j = 0; j < 4; ++j) {
				if (tetrahedron[j] == siteId) {
					tetrahedron[j] = -1;
					for (int k = (j+1) % 4; k != j; k = (k+1) % 4) {
						bool present = false;
						const int vertex = tetrahedron[k];
						if (vertex < 0 || vertex >= siteCount) continue;
						for (int i = 0; i < cutCount && !present; ++i) {
							present = present || vertex == cutSites[i];
						}
						if (!present) {
							cutSites[cutCount++] = vertex;
						}
					}
					break;
				}
			}
			
//			++cuttingIteration;
		}
		

		//------------------------------------------------------------------
		//use all relevant neighbours to cut
		//------------------------------------------------------------------



		for (int i = 0; i < cutCount; ++i) {
//			if (tetrahedron[i] == -1) continue;
//			cutSites[cutCount++] = tetrahedron[i];
//tetrahedron[i] * 3;
			int oppSiteIndex = cutSites[i] * 3;
			
			
			ivec2 cutVertices[50];
			for (int i = 0; i < 50; ++i) cutVertices[i] = ivec2(-1,-1);
			
			
			vec3 triangulationSite = vec3(sites[oppSiteIndex+0], sites[oppSiteIndex+1], sites[oppSiteIndex+2]);
			vec3 planePoint = (triangulationSite + mySite) * 0.5f;
			vec3 planeNormal = normalize(triangulationSite - mySite);
			float planeConst = -(dot(planePoint, planeNormal));
			vec4 plane = vec4(planeNormal, planeConst);
			


			//------------------------------------------------------------------
			//cut all edges
			//------------------------------------------------------------------


			for (int edgeIndex = 0; edgeIndex < edgeCount; ++edgeIndex) {
				int outside = 0;
				const ivec4 edge = edges[edgesOffset+edgeIndex];
				if (edge.x == edge.y) continue;
//				vec4 linePoints[2] = {auxVertices[auxVerticesOffset+edge[0]], auxVertices[auxVerticesOffset+edge[1]]};
				vec4 linePoints[2] = {auxVerticesTest[edge[0]], auxVerticesTest[edge[1]]};
				
				const float pointOrientation[2] = {
					dot(plane, linePoints[0]),
					dot(plane, linePoints[1])
				};
				
				outside |= int(pointOrientation[0] > uDelta) << 0;
				outside |= int(pointOrientation[1] > uDelta) << 1;
				
				bool needsCutting = false;
				int inIndex = 0;
				int outIndex = 0;
				
				switch (outside) {
				case 0:
					break;
				case 1:
					inIndex = 1;
					outIndex = 0;
					needsCutting = true;
					break;
				case 2:
					inIndex = 0;
					outIndex = 1;
					needsCutting = true;
					break;
				case 3:
					edges[edgesOffset+edgeIndex] = ivec4(0,0,-1,-1);
					break;
					//nastav stary edge jako prazdy - idealne zapamatuj at se to da potom znova pouzit - nejaky buffer vlastni s tim by byl treba...???
				default:
					break;
				}
				

				//------------------------------------------------------------------
				//split edge create new vertex
				//------------------------------------------------------------------




				if (needsCutting) {
					const vec4 inVertex = linePoints[inIndex];//vertices[verticesOffset+edge[inIndex]];
					const vec4 outVertex = linePoints[outIndex];//vertices[verticesOffset+edge[outIndex]];
					const vec4 dir = inVertex - outVertex;
					const vec4 newVertex = (dot(-plane, inVertex) / dot(plane, dir) * dir) + inVertex;
					
					
					//uloz novy bod na spravne misto - jak toto? proste mit counter
					
					const int newVertexIndex = vertexCount;
//					auxVertices[auxVerticesOffset+newVertexIndex] = newVertex;
					auxVerticesTest[newVertexIndex] = newVertex;
					++vertexCount;
					
					
					//nahrad stary bod novym
					//stare body nejak rusit?
					edges[edgesOffset+edgeIndex][outIndex] = newVertexIndex;
					
					if (cutVertices[edge.z].x == -1) cutVertices[edge.z].x = newVertexIndex;
					else if (cutVertices[edge.z].y == -1) cutVertices[edge.z].y = newVertexIndex;
					
					if (cutVertices[edge.w].x == -1) cutVertices[edge.w].x = newVertexIndex;
					else if (cutVertices[edge.w].y == -1) cutVertices[edge.w].y = newVertexIndex;
					
					//updatuj informace o nejvzdalenejsim bodu?
					//zapamatuj si novy bod pro vytvoreni nove steny
					
					
				}
			}
			
			//------------------------------------------------------------------
			//connect up the cut vertices to fix up the faces
			//------------------------------------------------------------------


			//connect cut face
			const int newFaceIndex = faceCount++;
			for (int i = 0; i < 50; ++i) {
				const int newEdgeIndex = edgeCount++;
				if (cutVertices[i].x != cutVertices[i].y && cutVertices[i].x != -1 && cutVertices[i].y != -1) {
					edges[edgesOffset+newEdgeIndex] = ivec4(cutVertices[i].x, cutVertices[i].y, i, newFaceIndex);
				}
			}
		}
		
		
		
//		for (int i = 0; i < 200; ++i) {
//			commands[i+5] = cutSites[i];
//			commands[i+5] = sites[i].x;
//			commands[i+5] = triangulation[i].x;
//		}
//		commands[20] = -1;
		
		
		
		
		//------------------------------------------------------------------
		//convert polygons into triangles
		//------------------------------------------------------------------

		//vytvareni indexu a commandu by asi mohlo byt vice paralelizovano... tim by se mohlo dat zamezit vysokym narokum na pamet a neznamemu poctu prvku ve startingPoints
		int usedVerticesCount = 0;
		int usedIndexCount = 0;
		int vertexMapping[200];
		vec4 usedVertices[200];
		int usedIndices[600];
		int startingPoints[200];
		
		for (int i = 0; i < 200; ++i) { startingPoints[i] = -1; vertexMapping[i] = -1; usedVertices[i] = vec4(0,0,0,1); }
		for (int i = 0; i < 600; ++i) { usedIndices[i] = -1; }
		
		for (int i = 0; i < edgeCount; ++i) {
			ivec4 edge = edges[edgesOffset+i];
			if (edge.x == edge.y) continue;
			int startingPoint1 = startingPoints[edge.z];
			int startingPoint2 = startingPoints[edge.w];
			int vertexMapping1 = vertexMapping[edge.x];
			int vertexMapping2 = vertexMapping[edge.y];
	//		int mapping3 = ;
//			int mapping4 = ;
			
			
			if (vertexMapping1 == -1) {
//				usedVertices[usedVerticesCount] = auxVertices[auxVerticesOffset+edge.x];
				usedVertices[usedVerticesCount] = auxVerticesTest[edge.x];
				vertexMapping1 = usedVerticesCount++;
				vertexMapping[edge.x] = vertexMapping1;
			}
			if (vertexMapping2 == -1) {
//				usedVertices[usedVerticesCount] = auxVertices[auxVerticesOffset+edge.y];
				usedVertices[usedVerticesCount] = auxVerticesTest[edge.y];
				vertexMapping2 = usedVerticesCount++;
				vertexMapping[edge.y] = vertexMapping2;
			}
			
			if (startingPoint1 == -1) {
				startingPoints[edge.z] = edge.x;//vertexMapping1;
			} else if(startingPoint1 != edge.x && startingPoint1 != edge.y) {
				//create indices
				usedIndices[indexCount++] = vertexMapping[startingPoint1];
				usedIndices[indexCount++] = vertexMapping1;//edge.x;
				usedIndices[indexCount++] = vertexMapping2;//edge.y;
			}
			if (startingPoint2 == -1) {
				startingPoints[edge.w] = edge.x;//vertexMapping1;
			} else if(startingPoint2 != edge.x && startingPoint2 != edge.y) {
				//create indices
				usedIndices[indexCount++] = vertexMapping[startingPoint2];
				usedIndices[indexCount++] = vertexMapping1;//edge.x;
				usedIndices[indexCount++] = vertexMapping2;//edge.y;
				//indices[indicesOffset+indexCount++] = vertexMapping[startingPoint2];
				//indices[indicesOffset+indexCount++] = vertexMapping1;//edge.x;
				//indices[indicesOffset+indexCount++] = vertexMapping2;//edge.y;
			}
			
			
		}

		//------------------------------------------------------------------
		//save the data for render and save metadata
		//------------------------------------------------------------------

		
		//allocate space for indices and vertices
		//copy indices and vertices
		const int indicesOffset = atomicAdd(info[indexFreeSpaceIndex], indexCount);
		const int verticesOffset = atomicAdd(info[vertexFreeSpaceIndex], usedVerticesCount);
		
		const int testOffset = atomicAdd(info[testIndex0], vertexCount + 1);
		atomicAdd(info[testIndex1], edgeCount);
		
//		for (int i = 0; i < vertexCount; ++i) {
//			test2[testOffset+i] = auxVerticesTest[i];
//		}
//		test2[testOffset+vertexCount] = vec4(-1,-1,-1,-1);
		
		for (int i = 0; i < indexCount; ++i) {
			indices[indicesOffset+i] = usedIndices[i];
		}
		
		for (int i = 0; i < usedVerticesCount; ++i) {
			vertices[verticesOffset+i] = usedVertices[i];
//			vertices[verticesOffset+i] = auxVerticesTest[i];
		}
		
//		for (int i = 0; i < 200; ++i) {
//			test1[200*siteId+i] = vertexMapping[i];
//		}
		
		
//		command = {indexCount, 1, indexTotal, 0, index};
		const int commandOffset = int(siteId) * 5;
//		const int commandOffset = 0 * 5;
		commands[commandOffset+0] = indexCount;// number of indices to render;
		commands[commandOffset+1] = 1;// instance to render
		commands[commandOffset+2] = indicesOffset;// offset of first index;
		commands[commandOffset+3] = verticesOffset;// offset of first vertex;
		commands[commandOffset+4] = int(siteId);// first and only instance id
		
		atomicAdd(info[testIndex3], cutCount);
		atomicMax(info[testIndex2], edgeCount);
		atomicMax(info[testIndex4], vertexCount);
	}
}
).";



//------------------------------------------------------------------
//test shader
//------------------------------------------------------------------


static const char testSetupSrc[] = R".(
#version 450

layout(binding=0)buffer Sites{vec4 sites[];};
layout(binding=1)buffer Triangulation{int triangulation[];};
layout(binding=2)buffer Info{int info[];};
layout(binding=3)buffer Vertices{vec4 vertices[];};
layout(binding=4)buffer Indices{int indices[];};
layout(binding=5)buffer Commands{int commands[];};

layout(local_size_x=32)in;

void main() {
	uint x = gl_GlobalInvocationID.x;
	
	float testVertices[] = {
		0.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
	};
	
	int testIndices[] = {
		0,1,2,
		0,1,3,
		0,2,3,
		1,2,3,
	};
	
	int testCommands[] = {
		12, 1, 0, 0, 0,
	};
	
	float testOffsets[] = {
		0.1f, 0.1f, 0.2f, 1.0f,
	};
	
	if (x == 0) {
		sites[0] = vec4(testOffsets[0], testOffsets[1], testOffsets[2], testOffsets[3]);
		for (int i = 0; i < 4; ++i) {
			vertices[i] = vec4(testVertices[i*4+0], testVertices[i*4+1], testVertices[i*4+2], testVertices[i*4+3]); 
		}
		for (int i = 0; i < 12; ++i) {
			indices[i] = testIndices[i];
		}
		for (int i = 0; i < 5; ++i) {
			commands[i] = testCommands[i];
		}
	}
	
}
).";


} // end namespace GFlipAddon

