//autor: Michael Ončo
#include <gDel3D/CommonTypes.h>


namespace GFlipAddon {

void transformAndRender(Point3HVec &points, GDelOutput &triangulation);

float transform(Point3HVec &points, GDelOutput &triangulation);
void render();

void testPrint();

} // end namespace GFlipAddon

