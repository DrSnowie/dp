//autor: Michael Ončo
#include "voronoi.h"
#include "shaders.h"

#include <stdio.h>
#include <chrono>

#include <glwindow.h>
#include <vertexarray.h>
#include <buffer.h>
#include <shader.h>
#include <program.h>
#include <orbitmanipulator.h>

using namespace OGL;

namespace GFlipAddon {

static GLWindow *window = nullptr;

static OrbitManipulator *orbitManipulator = nullptr;

static Program *renderProgram = nullptr;
static Program *setupProgram = nullptr;

static Shader *vShader = nullptr;
static Shader *gShader = nullptr;
static Shader *fShader = nullptr;
static Shader *setupShader = nullptr;

static Buffer *vertexBuffer = nullptr;
static Buffer *indexBuffer = nullptr;
static Buffer *commandBuffer = nullptr;

static Buffer *infoBuffer = nullptr;
static Buffer *offsetBuffer = nullptr;
static Buffer *triangulationBuffer = nullptr;
static Buffer *edgeBuffer = nullptr;
static Buffer *vertexAuxBuffer = nullptr;

static Buffer *testBuffer1 = nullptr;
static Buffer *testBuffer2 = nullptr;

static VertexArray *VAO = nullptr;

static float cellDistance = 0;
static bool rightMouseDown =  false;
static float sensitivity = 0.1f;

static int renderSize = 0;





static float init(Point3HVec &points, TetHVec &triangulation) {


	//------------------------------------------------------------------
	//initialize data
	//------------------------------------------------------------------



	window = new GLWindow;
	
	orbitManipulator = new OrbitManipulator;
	
	renderProgram = new Program;
	setupProgram = new Program;
	
	vShader = new Shader(GL_VERTEX_SHADER, vSrc);
	gShader = new Shader(GL_GEOMETRY_SHADER, gSrc);
	fShader = new Shader(GL_FRAGMENT_SHADER, fSrc);
	setupShader = new Shader(GL_COMPUTE_SHADER, setupSrc);
	
	
	renderProgram->attachShader(vShader);
	renderProgram->attachShader(gShader);
	renderProgram->attachShader(fShader);
	renderProgram->link();
	
	setupProgram->attachShader(setupShader);
	setupProgram->link();
	
	vertexBuffer = new Buffer(GL_DYNAMIC_READ);
	indexBuffer = new Buffer(GL_DYNAMIC_READ);
	commandBuffer = new Buffer(GL_DYNAMIC_READ);
	infoBuffer = new Buffer(GL_DYNAMIC_READ);
	offsetBuffer = new Buffer(GL_DYNAMIC_READ);
	triangulationBuffer = new Buffer(GL_DYNAMIC_READ);
	edgeBuffer = new Buffer(GL_DYNAMIC_READ);
	vertexAuxBuffer = new Buffer(GL_DYNAMIC_READ);
	
	testBuffer1 = new Buffer(GL_DYNAMIC_READ);
	testBuffer2 = new Buffer(GL_DYNAMIC_READ);
	
	glClearColor(0.2, 0.2, 0.2, 1);
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	
	float initialVertices[] = {
		 0,  0,  0,  1,
		10,  0,  0,  1,
		 0, 10,  0,  1,
		 0,  0, 10,  1,
	};
	
	int initialEdges[] = {
		0,1,2,
		0,2,3,
		0,1,3,
		1,2,3,
	};
	
	
	Tet *t = triangulation.data();
	Point3 *p = points.data();
	const int pSize = points.size();
	const int tSize = triangulation.size();
	
	
	VAO = new VertexArray();
	VAO->bind();
	
	
	int estimatedEdges = 5000;
	int estimatedAuxVertices = 1000;
	
	int estimatedUsedVertices = 1000;
	int estimatedUsedIndices = 1000;
	int estimatedCutSites = 1000;
	int estimatedStartingVertice = 1000;
	int estimatedVertexMapping = 1000;
	
	int estimatedVertices = 100;
	int estimatedTriangles = 100;
	
	int groupCount = 16;
	int totalThreadCount = groupCount * 32;
	int tetrahedronSize = 30; //size of tetrahedron, to get real value divide by ten
	
	int estimatedEdgesSpace = estimatedEdges * totalThreadCount;
	int estimatedAuxVerticesSpace = estimatedAuxVertices * totalThreadCount;
	
	int estimatedVerticesSpace = estimatedVertices * pSize;
	int estimatedTrianglesSpace = estimatedTriangles * pSize;
	
	renderSize = pSize;
	int initialInfo[] = {
		renderSize,             //0
		tSize,                  //1
		estimatedVertices,      //2
		estimatedEdges,         //3
		3 * estimatedTriangles, //4
		estimatedAuxVertices,   //5
		0,                      //6
		0,                      //7
		0,                      //8
		0,                      //9
		0,                      //10
		0,                      //11
		tetrahedronSize,        //12
		0,                      //13
	};
	


	//------------------------------------------------------------------
	//initialize buffers, move triangulation onto GPU for OpenGL to use - check time just to see -> dont measure for real
	//------------------------------------------------------------------


	
	auto t0 = std::chrono::steady_clock::now();
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	offsetBuffer->createMutable(pSize * 3 * sizeof(float), p);
	
	triangulationBuffer->bind(GL_SHADER_STORAGE_BUFFER, 1);
	triangulationBuffer->createMutable(tSize * 4 * sizeof(int), t);
	auto t1 = std::chrono::steady_clock::now();
	
	//TODO creation and filling of these buffers shouldn't be included in total time as this should be already on GPU if done in other way
	infoBuffer->bind(GL_SHADER_STORAGE_BUFFER, 2);
	infoBuffer->createMutable(sizeof(initialInfo), initialInfo);
	
	vertexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 3);
	vertexBuffer->allocateMutable(estimatedVerticesSpace * sizeof(glm::vec4));
//	vertexBuffer->setData(initialVertices, sizeof(initialVertices));
	
	indexBuffer->bind(GL_SHADER_STORAGE_BUFFER, 4);
	indexBuffer->allocateMutable(estimatedTrianglesSpace * sizeof(int) * 3);
	
	commandBuffer ->bind(GL_SHADER_STORAGE_BUFFER, 5);
	commandBuffer->allocateMutable(pSize * 5 * sizeof(int));
	
	edgeBuffer->bind(GL_SHADER_STORAGE_BUFFER, 6);
	edgeBuffer->allocateMutable(estimatedEdgesSpace * 4 * sizeof(int));
	
	vertexAuxBuffer->bind(GL_SHADER_STORAGE_BUFFER, 7);
	vertexAuxBuffer->allocateMutable(estimatedAuxVerticesSpace * 4 * sizeof(float));
	
	testBuffer1->bind(GL_SHADER_STORAGE_BUFFER, 8);
	testBuffer1->allocateMutable(4000);
	
	testBuffer2->bind(GL_SHADER_STORAGE_BUFFER, 9);
	testBuffer2->allocateMutable(4000);
	
	auto t2 = std::chrono::steady_clock::now();
	
	const float time1 = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() / 1000000.0f;
	const float time2 = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / 1000000.0f;
	
//	printf("\nmeasured times:\n");
//	printf("===========================================\n");
//	printf("sites and triangulation load time: %10.3f ms\n", time1);
//	printf("buffer load time:                  %10.3f ms\n", time2);
	


	//------------------------------------------------------------------
	//check memory usage
	//------------------------------------------------------------------



	const int totalByteSize =
		(pSize * 3
		+tSize * 4
		+estimatedVerticesSpace * 4
		+estimatedTrianglesSpace * 3
		+pSize * 5
		+estimatedEdgesSpace * 4
		+estimatedAuxVerticesSpace * 4);
//		*4;
	
	printf("\nbuffer sizes\n");
	printf("===========================================\n");
	printf("size sites:         %14.3f kB\n", pSize * 3 * sizeof(float) / 1024.0f);
	printf("size triangulation: %14.3f kB\n", tSize * 4 * sizeof(int) / 1024.0f);
	printf("size vertices:      %14.3f kB\n", estimatedVerticesSpace * sizeof(glm::vec4) / 1024.0f);
	printf("size indices:       %14.3f kB\n", estimatedTrianglesSpace * 3 * sizeof(int) / 1024.0f);
	printf("size commands:      %14.3f kB\n", pSize * 5 * sizeof(int) / 1024.0f);
	printf("size edges:         %14.3f kB\n", estimatedEdgesSpace * 4 * sizeof(int) / 1024.0f);
	printf("size vertices aux:  %14.3f kB\n", estimatedAuxVerticesSpace * 4 * sizeof(float) / 1024.0f);
//	printf("size vertices aux:  %14.3f kB\n", estimatedAuxVerticesSpace * 4 * sizeof(float) / 1024.0f);
	printf("total size:         %14.3f kB\n", 4 * totalByteSize / 1024.0f);
//	printf("triangulation size: %14.3f \n, ");
	


	//------------------------------------------------------------------
	// compute the VD
	//------------------------------------------------------------------

	
//	GLuint tQuery;
//	GLuint64 queryTime;
//	glGenQueries(1, &tQuery);
	
	setupProgram->useProgram();
	
	glFinish();
	auto t3 = std::chrono::steady_clock::now();
//	glBeginQuery(GL_TIME_ELAPSED, tQuery);
	
//	glDispatchCompute(1,1,1);
	glDispatchCompute(groupCount,1,1);
	
	glFinish();
	auto t4 = std::chrono::steady_clock::now();
//	glEndQuery(GL_TIME_ELAPSED);
//	glGetQueryObjectui64v(tQuery, GL_QUERY_RESULT, &queryTime);
//	const float time = queryTime / 1000000.0f;
	const float time = std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count() / 1000000.0f;
	


	//------------------------------------------------------------------
	//set buffers to be used in render and setup input and render functions
	//------------------------------------------------------------------

	
	vertexBuffer->bind(GL_ARRAY_BUFFER);
	indexBuffer->bind(GL_ELEMENT_ARRAY_BUFFER);
	commandBuffer->bind(GL_DRAW_INDIRECT_BUFFER);
	offsetBuffer->bind(GL_SHADER_STORAGE_BUFFER, 0);
	triangulationBuffer->bind(GL_SHADER_STORAGE_BUFFER, 1);
	infoBuffer->bind(GL_SHADER_STORAGE_BUFFER, 2);
	 
	VAO->addAttribute(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	renderProgram->useProgram();
	
	//------------------------------------------------------------------
	//uses its own window from time this code was separate -> this should be changed
	//------------------------------------------------------------------



	window->addInputFunction(orbitManipulator->inputFunction);
	window->addInputFunction([&](SDL_Event &e) -> int {
		unsigned int key;
		switch (e.type) {
		case SDL_KEYDOWN:
			key = e.key.keysym.sym;
			
			switch (key) {
			case SDLK_q:
				return 1;
				break;
			case SDLK_RIGHT:
				if (renderSize < 64) renderSize++;
				break;
			case SDLK_LEFT:
				if (renderSize > 0) renderSize--;
				break;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_RIGHT) {
				rightMouseDown = false;
			}
			break;
		case SDL_MOUSEMOTION:
			if (rightMouseDown) {
				cellDistance += e.motion.xrel * sensitivity;
				cellDistance = cellDistance < 0.0f ? 0.0f : cellDistance;
			}
			break;
		default:
			break;
		}
		return 0;
	});
	
	window->addRenderFunction([&]() -> int {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		renderProgram->uniformMat4("uVP", orbitManipulator->matrix);
		renderProgram->uniformFloat("uCellDistance", cellDistance);
		
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, renderSize, 0);
		
		return 0;
	});
	
	return time;
}





static void destroy() {
	delete window;
	
	delete renderProgram;
	delete setupProgram;
	
	delete vShader;
	delete gShader;
	delete fShader;
	delete setupShader;
	
	delete VAO;
	
	delete vertexBuffer;
	delete indexBuffer;
	delete commandBuffer;
	delete infoBuffer;
	delete offsetBuffer;
	delete triangulationBuffer;
	delete edgeBuffer;
	delete vertexAuxBuffer;
	delete testBuffer1;
	delete testBuffer2;
}





float transform(Point3HVec &points, GDelOutput &triangulation) {
	int max = points.size();
	
//	printf("\nvertices\n");
//	printf("===========================================\n");
//	for (int i = 0; i < max; ++i) {
//		const glm::vec3 p(points[i]._p[0], points[i]._p[1], points[i]._p[2]);
//		printf("%4d: %.8f, %.8f, %.8f, length: %.8f\n", i, p.x, p.y, p.z, glm::dot(p,p));
//	}
	
	max = triangulation.tetVec.size();
	Tet *t = triangulation.tetVec.data();
	
//	printf("\nindices\n");
//	printf("===========================================\n");
//	for (int i = 0; i < max; ++i) {
//		printf("%4d: %9d, %9d, %9d, %9d\n", i, t[i]._v[0], t[i]._v[1], t[i]._v[2], t[i]._v[3]);
//	}
	
	const float time = init(points, triangulation.tetVec);
	
	return time;
}





void render() {
	window->run();
	destroy();
}





void testPrint() {
	int *intData = new int[1000000];
	float *floatData = new float[1000000];
	
	infoBuffer->getData(intData, 14 * sizeof(int));
	printf("\ninfo\n");
	printf("===========================================\n");
	printf(" 0: siteCount:            %9d\n", intData[0]);
	printf(" 1: tetrahedronCount:     %9d\n", intData[1]);
	printf(" 2: estimatedVertices:    %9d\n", intData[2]);
	printf(" 3: estimatedEdges:       %9d\n", intData[3]);
	printf(" 4: estimatedIndices:     %9d\n", intData[4]);
	printf(" 5: estimatedAuxVertices: %9d\n", intData[5]);
	printf(" 6: usedVertexCount:      %9d\n", intData[6]);
	printf(" 7: usedIndexCount:       %9d\n", intData[7]);
	printf(" 8: totalVertexCount:     %9d\n", intData[8]);
	printf(" 9: totalEdgeCount:       %9d\n", intData[9]);
	printf("10: maxEdgeCount:         %9d\n", intData[10]);
	printf("11: cutsAvg:              %12.2f\n", (float)intData[11]/intData[0]);
	printf("13: maxVertCount:         %9d\n", intData[13]);
	
	int indexCount = intData[7];
	int vertexCount = intData[6];
	int testCount = intData[8];
	int edgeCount = intData[9];

	//------------------------------------------------------------------
	//disabled test prints... probably will be needed
	//------------------------------------------------------------------


/*	
	commandBuffer->getData(intData, 100 * sizeof(int));
	printf("command\n");
	for (int i = 0; i < 2*5; i+=5) printf("%d: %d,%d,%d,%d,%d\n", i/5, intData[i+0], intData[i+1], intData[i+2], intData[i+3], intData[i+4]);
//	indexCount = intData[0+5] / 3;
//	indexStart = intData[2+5] / 3;
	indexBuffer->getData(intData, 1000 * sizeof(int));
	printf("indices\n");
	for (int i = 0; i < indexCount; i += 3) printf("%d = %d : %d : %d\n", i, intData[i+0], intData[i+1], intData[i+2]);
	
	
	vertexBuffer->getData(floatData, 1000 * sizeof(float));
	printf("vertices\n");
	for (int i = 0; i < vertexCount; ++i) printf("%d = %f : %f : %f : %f\n", i, floatData[i*4+0], floatData[i*4+1], floatData[i*4+2], floatData[i*4+3]);
//	printf("cutSites\n");
//	for (int i = 0; i < 20; ++i) printf("%d:%d\n", i, intData[i+5]);
//	int j = 0;
//	for (int i = 0; i < tSize; ++i) if (t[i]._v[0] == 0 || t[i]._v[1] == 0 || t[i]._v[2] == 0 || t[i]._v[3] == 0) printf("%d:%d: %d,%d,%d,%d\n", i, j++, t[i]._v[0], t[i]._v[1], t[i]._v[2], t[i]._v[3]);
	
	
	edgeBuffer->getData(intData, 1000 * 2 * 4 * sizeof(int));
	printf("edge\n");
	for (int i = 0; i < 1000 * 2; ++i) if (intData[i*4+0] != intData[i*4+1]) printf("%d = %d:%d ::: %d:%d\n",i, intData[i*4+0], intData[i*4+1], intData[i*4+2], intData[i*4+3]);
	
	printf("all vertices\n");
	testBuffer2->getData(floatData, testCount * sizeof(glm::vec4));
	for (int i = 0; i < testCount; ++i) printf("%d = %f : %f : %f : %f\n", i, floatData[i*4+0], floatData[i*4+1], floatData[i*4+2], floatData[i*4+3]);
	
	printf("vertex mappings\n");
	testBuffer1->getData(intData, 400 * sizeof(int));
	for (int i = 0; i < 400; ++i) printf("%d: %d\n", i, intData[i+0]);
*/	
	
	
//	printf("edgeCOUNT: %d\n", edgeCount);
//	for (int i = 0; i < 400; ++i) if (intData[i] != -1) printf("%d: %d\n", i, intData[i+0]);
	
/*	
	glm::vec3 min = { 99999,  99999,  99999};
	glm::vec3 max = {-99999, -99999, -99999};
	
	for (int i = 0; i < pSize; ++i) {
//		printf("%f, %f, %f\n", p[i]._p[0], p[i]._p[1], p[i]._p[2]);
		if (min.x > p[i]._p[0]) min.x = p[i]._p[0];
		if (max.x < p[i]._p[0]) max.x = p[i]._p[0];
		
		if (min.y > p[i]._p[1]) min.y = p[i]._p[1];
		if (max.y < p[i]._p[1]) max.y = p[i]._p[1];
		
		if (min.z > p[i]._p[2]) min.z = p[i]._p[2];
		if (max.z < p[i]._p[2]) max.z = p[i]._p[2];
	}
	
	printf("min: %f : %f : %f\nmax: %f : %f : %f\n", min.x, min.y, min.z, max.x, max.y, max.z);
*/	
	
	delete intData;
	delete floatData;
}





void transformAndRender(Point3HVec &points, GDelOutput &triangulation) {
	init(points, triangulation.tetVec);
	
	window->run();
	destroy();
}

} // end namespace GFlipAddon

