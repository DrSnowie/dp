Autor: Michael Onco
Datum: 30.7.2020
Projekt: FIT VUT Diplomová práce: Rozklad těles Voroného diagramem na GPU


Obsah:
bin/             - složka se spustitelným souborem projektu
doc/             - složka se zdrojovým kódem dokumentace
GDelFlipping/    - prázdná složka pro vložení zdrojového kódu algoritmu GDel3D
gflipaddon/      - složka se zdrojovým kódem modulu GFlipAddon
render/          - složka se zdrojovým kódem modulu OGL
src/             - složka se zdrojovým kódem modulu Main a VST
CMakeLists.txt   - soubor definující strukturu projektu pro nástroj CMake
demo.mp4         - video prezentující tento projekt
README.md        - soubor se základními informcemi o projektu
runscript        - skript pro zjednodušení spouštění výsledného programu, 
                   program je očekáván pod jménem dp buď ve složce build nebo ve stejné složce, 
                   ve které se nachází skript


Stránka použitá k vytváření obrázků v rámci DP: https://www.desmos.com/calculator/rioincjc7e


Kompilace programu:
Pro úspěšnou kompilaci jsou vyžadovány knihovny GLM, GLEW a SDL2, také je potřeba nástroj CMake
Pokud je vyžadována i kompilace modulu gflipaddon je potřeba v CMake zapnout přepínač gFlip3D.
Tento modul společně s implementací GDel3D je pak zakomponován do konečného programu.
Pro zprovoznění tohoto module je očekáván přesun obsahu složky GDelFlipping z implementace GDel3D 
do složky stejného jména přítomné v tomto projektu. Pro úspěšné zprovoznění GDel3D bude možná 
potřeba upravit CMake. Pro potenciální postup viz:

https://stackoverflow.com/questions/61114875/

a implementace GDel3D je dostupná na:

https://www.comp.nus.edu.sg/~tants/gdel3d_files/gDel3D-Release_271.zip


Doporučený postup kompilace pak je:

mkdir build
cd build
cmake ..
make

Pro windows využijte grafické rozhraní nástroje CMake a svůj oblíbený způsob překladu.


Spustitelné binární soubory z bin/:
V této složce jsou 3 kombinace binárních souborů. První ihned po vstupu do složky viditelná je kombinace 
pro systém Windows. Ta byla testována a je funkční. Neobsahuje však zkompilovanou část GFlipAddon a GDel3D.
Pak jsou zde ještě dvě složky se soubory pro systém Linux, ty bohužel nebyly testovány. Ve složce 
linuxwithgflipaddon/ je zkompilovaná verze s GDel3D a ve složce linuxwithoutgflipaddon/ je verze odpovídající
té Windowsové.



Argumenty:
Výsledná aplikace přijíma buď žádné argumenty, nebo přesně tři v následujícím pořadí:

1) první číslo nastaví metodu, která má být použita pro vytvoření Voroného diagramu: 
         0 - implementace VST GPU
         1 - implementace GDel3D na VD pokud je zprovozněn modul GFlipAddon
         2 - implementace VST CPU
2) druhé číslo nastaví počet pracovních skupin zajišťující vytváření Voroného diagramu
3) třetí číslo nastaví počet vstupních bodů pro vytvoření Voroného diagramu



Ovládání:
LEVÉ TLAČÍTKO MYŠI  - pohyb myši ovládá rotaci kolem tříštěného objektu
PRAVÉ TLAČÍTKO MYŠI - pohyb myši po horizontální ose ovládá vzdálenost mezi buňkami Voroného diagramu
KOLEČKO MYŠI        - točení kolečka ovládá pohyb kamery dopředu a dozadu - přiblížení a oddálení k objektu
Q                   - ukončení aplikace


Výstupem programu je nejen grafické interaktivní okno, ale také výstup v konzoli 
s časy vytváření Voroného diagramu a dalšími informacemi.



Výpisy:
===========================================================================================================================================
VST CPU
===========================================================================================================================================
number of sites:                                                        - počet bodů ve vstupní množině
elapsed time from start of cutting to the end of upload to GPU:  ms     - čas, který uplinul mezi zahájením okrajování 
                                                                          a okamžikem kdy je GPU připraveno k vykreslování výsledných dat

===========================================================================================================================================
VST GPU
===========================================================================================================================================
calculated memory size:
-------------------------------
vertexBuffer size:  kB                                        - velikost paměti vymezená vrcholům přímo pro render = rohy buněk
indexBuffer size:  kB                                         - velikost paměti vymezená indexům přímo pro render
edgesBuffer size:  kB                                         - velikost místa v paměti pro všechny vlákna = hrany = bývá největší žrout paměti
siteDistancesBuffer size:  kB                                 - velikost místa v paměti přidělená vzdálenostem mezi jednotlivými jádry
vertexDistancesBuffer size:  kB                               - velikost místa v paměti vymezené vzdálenostem bodů buňky
siteBuffer size:  kB                                          - velikost místa v paměti zabrané vstupní množinou bodů
commandsBuffer size:  kB                                      - velikost celého prostoru přiděleného příkazům = indirect buffer
auxIntBuffer size:  kB                                        - velikost pomocného bufferu s celými čísli
auxVecBuffer size:  kB                                        - velikost pomocného bufferu s vektory vec4
total:  kB                                                    - celkově zabraná paměť na GPU
-------------------------------
siteCount:                                                    - počet vstupních bodů
real time in ms:                                              - čas který trvalo vytváření VD, od spuštění shaderu po jeho ukončení
tetrahedron scale:                                            - velikost čtyřstěnu použitého v algoritmu = hodnota odpovida delce tri hran 
	                                                            ktere jsou na sebe kolme	
max edges:                                                    - maximální počet hran, které jedna buňka v rámci své tvorby vygenerovala
max vertices:                                                 - maximální počet vrcholů, které jedna buňka v rámci své tvorby vygenerovala
average cuts:                                                 - průměrný počet okrajování pro jednu buňku
amount of cells using all neighbours:                         - počet buněk, které využili okrajování všemi sousedy = naivní přístup

===========================================================================================================================================
GFlipAddon
===========================================================================================================================================
times measured
===============================
triangulation time:        ms                             - čas za, který byla vypočítána triangulace
delaunay to voronoi time:  ms                             - čas za, který byla převedena na VD
total:                     ms                             - celkový čas, který trval výpočet DT i převod na VD

buffer sizes
===========================================
size sites:         kB                                    - velikost paměti, kterou zabírají body vstupní množiny
size triangulation: kB                                    - velikost paměti, kterou zabírá vypočítaná triangulace
size vertices:      kB                                    - velikost pameti vyhrazená pro očekávané množství vrcholů pro vykreslení
size indices:       kB                                    - velikost paměti vyhrazená pro očekávané množství indexů pro vykreslení
size commands:      kB                                    - velikost paměti vyhrazená pro příkazy k vykreslení
size edges:         kB                                    - pomocná paměť vyhrazená pro předpokládané množství hran vygenerovaných během vytváření buněk,
                                                            odpovídá množství vláken krát očekávané množství hran na jedno buňku
size vertices aux:  kB                                    - pomocná paměť vyhrazená pro předpokládané množství vrcholů vygenerovaných během vytváření 
                                                            buněk, podobně jako položka výše
total size:         kB                                    - celková vypočítané velikost paměti těchto částí

info
===========================================
 0: siteCount:             - počet jader vstupní množiny
 1: tetrahedronCount:      - počet čtyřstěnů vzniklých v GDel3D
 2: estimatedVertices:     - očekávaný počet vrcholů k vykreslení na jádro
 3: estimatedEdges:        - očekávaný počet hran na jedno vlákno
 4: estimatedIndices:      - očekávaný počet indexů k vykreslení na jádro
 5: estimatedAuxVertices:  - očekávaný počet pomocných vrcholů vygenerovaných při okrajování na vlákno
 6: usedVertexCount:       - počet položek pomocného pole, pro ukládání bodů, které jsou na konci tvorby buňky aktivní
 7: usedIndexCount:        - počet položek pomocného pole, pro ukládání indexů, které jsou na konci tvorby buňky aktivní
 8: totalVertexCount:      - počet všech vytvořených vrcholů po převodu na VD
 9: totalEdgeCount:        - počet všech vytvořených hran po převodu na VD
10: maxEdgeCount:          - maximální počet hran vytvořených při vytváření jedné buňky
11: cutsAvg:               - průměrný počet ořezání buňky pro její vytvoření
13: maxVertCount:          - maximální počet vrcholů vytvořených při vytváření jedné buňky
	
	
