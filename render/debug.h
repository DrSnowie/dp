//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>



namespace OGL {

inline void printVec3(glm::vec3 vector, std::string text) {
	std::cout << text << ": (" << vector.x << "," << vector.y << "," << vector.z << ")\n";
}

inline void printMat4(glm::mat4 matrix, std::string text) {
	std::cout << text << ":\n";
	std::cout << "(" << matrix[0][0] << "," << matrix[0][1] << "," << matrix[0][2] << "," << matrix[0][3] << ")\n";
	std::cout << "(" << matrix[1][0] << "," << matrix[1][1] << "," << matrix[1][2] << "," << matrix[1][3] << ")\n";
	std::cout << "(" << matrix[2][0] << "," << matrix[2][1] << "," << matrix[2][2] << "," << matrix[2][3] << ")\n";
	std::cout << "(" << matrix[3][0] << "," << matrix[3][1] << "," << matrix[3][2] << "," << matrix[3][3] << ")\n";
}

inline bool debugGL() {
	int error;
	bool was = false;
	while ((error = glGetError()) != GL_NO_ERROR) {
		//printf("%s - %d || error '%d': %s\n", __FILE__, __LINE__, error, gluErrorString(error));
		printf("error '%d': %s\n", error, (char*)gluErrorString(error));
		was = true;
	}
	return was;
}

} // end namespace OGL
