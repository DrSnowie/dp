//autor: Michael Ončo
#pragma once
#include <SDL.h>
#include <SDL_opengl.h>
#include <list>
#include <memory>
#include <iostream>

namespace OGL {

class Window {
public:
//	Window();
	virtual ~Window();

	void open();
	// should be in some window manager or something so it can handle multiple windows... but fuck that for now
	int run();
	//void update();
	void close();

	static void init(Uint32 flags = SDL_INIT_VIDEO);
	static void quit();

	void setDimensions(Uint32 width, Uint32 height);
	void setHeight(Uint32 height);
	void setWidth(Uint32 width);
	void setName(std::string name);

	void setUpdateFunction();
	void setInputFunction();

private:
	Uint32 _windowHeight = 600;
	Uint32 _windowWidth = 800;

	//static std::list<std::shared_ptr<SDL_Window>> createdWindows;
	static bool _initialized;
	static Uint32 _windowsOpened;
	bool _windowOpened = false;

	std::string _windowName = "Window";
	SDL_Window* _window;
	//SDL_GLContext _context;

	//funkce na update
	//funkce render?
	//funkce na zpracovani vstupu
};

} // end namespace OGL

