//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <iostream>

namespace OGL {

void printProgramLog(GLuint program);
void printShaderLog(GLuint shader);

void GLAPIENTRY
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam);



} // end namespace OGL

