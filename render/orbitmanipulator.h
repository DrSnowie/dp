//autor: Michael Ončo
#pragma once
#include "camera.h"
#include <SDL2/SDL.h>
#include <glm/glm.hpp>

#include <functional>

namespace OGL {

class OrbitManipulator {
	
private:
	glm::vec3 _rotationPoint = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::mat4 _projectionMatrix = glm::mat4(1.0f);
	glm::mat4 _rotationMatrix = glm::mat4(1.0f);
	Camera *_camera = nullptr;
	glm::mat4 _matrix = glm::mat4(1.0f);

	bool _leftMouseDown = false;

	float _angleX = 0.0f;
	float _angleY = 0.0f;

	float _distance = 0.0f;
	float _rotationSensitivity = 0.005f;
	float _distanceSensitivity = 0.5f;
public:

	OrbitManipulator(Camera *camera);
	OrbitManipulator();

	void rotate(int x, int y);
	void changeDistance(int y);
	
	std::function<int(SDL_Event &e)> inputFunction = [](SDL_Event &e) { return 0; };
	const glm::mat4 &matrix = _matrix;
};

} // end namespace OGL

