//autor: Michael Ončo
#include "program.h"

namespace OGL {

Program::Program() {
	_program = glCreateProgram();
}

Program::~Program() {
	glDeleteProgram(_program);
}

void Program::attachShader(std::shared_ptr<Shader> shader) {
	glAttachShader(_program, shader->getShader());
	_linked = false;
}

void Program::attachShader(Shader *shader) {
	glAttachShader(_program, shader->getShader());
	_linked = false;
}

void Program::detachShader(Shader *shader) {
	glDetachShader(_program, shader->getShader());
	_linked = false;
}


void Program::link() {
	if (!_linked) {
		_linked = true;
		glLinkProgram(_program);
		GLint linking = GL_TRUE;
		glGetProgramiv(_program, GL_LINK_STATUS, &linking);
		if (linking != GL_TRUE)
		{
			std::cerr << "Error linking program " << _program << "!\n";
			printProgramLog(_program);
		}
	}
}

void Program::useProgram() {
	if (!_linked) {
		_linked = true;
//TODO segfault here
		glLinkProgram(_program);
		GLint linking = GL_TRUE;
		glGetProgramiv(_program, GL_LINK_STATUS, &linking);
		if (linking != GL_TRUE)
		{
			std::cerr << "Error linking program " << _program << "!\n";
			printProgramLog(_program);
		}
		glUseProgram(_program);
	} else {
		glUseProgram(_program);
	}
}

GLint Program::getAttributeLocation(const GLchar *name) {
	return glGetAttribLocation(this->_program, name);
}

//TODO save uniform locations into a map or something
void Program::uniformMat4(std::string name, const glm::mat4 &matrix) {
	GLuint uniformID = glGetUniformLocation(this->_program, name.c_str());
	glUniformMatrix4fv(uniformID, 1, GL_FALSE, &matrix[0][0]);
}

void Program::uniformVec4(std::string name, const glm::vec4 &vector) {
	GLuint uniformID = glGetUniformLocation(this->_program, name.c_str());
	glUniform4fv(uniformID, 1, &vector[0]);
}

void Program::uniformFloat(std::string name, const GLfloat number) {
	GLuint uniformID = glGetUniformLocation(this->_program, name.c_str());
	glUniform1f(uniformID, number);
}

void Program::uniformUint(std::string name, const GLuint number) {
	GLuint uniformID = glGetUniformLocation(this->_program, name.c_str());
	glUniform1ui(uniformID, number);
}











} // end namespace OGL

