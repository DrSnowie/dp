//autor: Michael Ončo
#include "buffer.h"

#include <iostream>

namespace OGL {

Buffer::~Buffer() {
	glDeleteBuffers(1, &_id);
}

Buffer::Buffer(GLenum usage) {
	_usage = usage;
	glGenBuffers(1, &_id);
}
Buffer::Buffer(GLenum usage, GLenum target, GLuint position, GLsizei size, GLvoid *data, GLsizei dataSize) {
	_usage = usage;
	glGenBuffers(1, &_id);
	bind(target, position);
	allocateMutable(size);
	setData(data, dataSize);
}
Buffer::Buffer(GLenum usage, GLenum target, GLsizei size, GLvoid *data, GLsizei dataSize) {
	_usage = usage;
	glGenBuffers(1, &_id);
	bind(target);
	allocateMutable(size);
	setData(data, dataSize);
}
Buffer::Buffer(GLenum usage, GLenum target, GLuint position, GLsizei size, GLvoid *data) {
	_usage = usage;
	glGenBuffers(1, &_id);
	bind(target, position);
	createMutable(size, data);
}
Buffer::Buffer(GLenum usage, GLenum target, GLsizei size, GLvoid *data) {
	_usage = usage;
	glGenBuffers(1, &_id);
	bind(target);
	createMutable(size, data);
}
/*Buffer::Buffer(GLenum usage, GLsizei size) {
	_usage = usage;
	printf("\n\ntest1 %d\n\n\n", _id);
	glGenBuffers(1, &_id);
	printf("\n\ntest2 %d\n\n\n", _id);
	allocateMutable(size, usage);
	printf("\n\ntest3\n\n\n");
}
Buffer::Buffer(GLenum usage, GLsizei size, GLvoid *data) {
	_usage = usage;
	glGenBuffers(1, &_id);
	createMutable(usage, size, data);
}
Buffer::Buffer(GLenum usage, GLsizei size, GLvoid *data, GLsizei dataSize) {
	_usage = usage;
	glGenBuffers(1, &_id);
	allocateMutable(size);
	setData(data, dataSize);
}*/

void Buffer::bind(GLenum target, GLuint position) {
	glBindBufferBase(target, position, _id);
//	_target = target;
}
void Buffer::bind(GLenum target) {
/*	if (target == GL_ELEMENT_ARRAY_BUFFER) {
//		printf("elementBuffer %d", this->_id);
	} else if (target == GL_ARRAY_BUFFER) {
//		printf("arrayBuffer %d", this->_id);
	}*/
	glBindBuffer(target, _id);
//	_target = target;
}
/*void Buffer::bind(GLuint position) {
	glBindBufferBase(_target, position, _id);
}*/
/*void Buffer::bind() {
	glBindBuffer(_target, _id);
}*/

/*void Buffer::setTarget(GLenum target) {
	_target = target;
}*/
void Buffer::createMutable(GLenum usage, GLsizei size, GLvoid *data, GLsizei dataSize) {
	_mutable = true;
	_usage = usage;
	glNamedBufferData(_id, size, nullptr, _usage);
	setData(data, dataSize);
}
void Buffer::createMutable(GLsizei size, GLvoid *data, GLsizei dataSize) {
	_mutable = true;
	glNamedBufferData(_id, size, nullptr, _usage);
	setData(data, dataSize);
}
void Buffer::createMutable(GLenum usage, GLsizei size, GLvoid *data) {
	_mutable = true;
	_usage = usage;
	glNamedBufferData(_id, size, data, _usage);
}
void Buffer::createMutable(GLsizei size, GLvoid *data) {
	_mutable = true;
	glNamedBufferData(_id, size, data, _usage);
}
void Buffer::allocateMutable(GLenum usage, GLsizei size) {
	_mutable = true;
	_usage = usage;
	glNamedBufferData(_id, size, nullptr, _usage);
}
void Buffer::allocateMutable(GLsizei size) {
	_mutable = true;
	glNamedBufferData(_id, size, nullptr, _usage);
}
/*void Buffer::allocateMutable(GLsizei size, GLenum usage, GLenum target) {
	_mutable = true;
	_usage = usage;
	_target = target;
	glNamedBufferData(_id, size, nullptr, _usage);
}*/
void Buffer::createImutable(GLenum usage, GLsizei size, GLvoid *data) {
	_mutable = false;
	_usage = usage;
	glNamedBufferStorage(_id, size, data, _usage);
}
void Buffer::createImutable(GLsizei size, GLvoid *data) {
	_mutable = false;
	glNamedBufferStorage(_id, size, data, _usage);
}
void Buffer::allocateImutable(GLenum usage, GLsizei size) {
	_mutable = false;
	_usage = usage;
	glNamedBufferStorage(_id, size, nullptr, _usage);
}
void Buffer::allocateImutable(GLsizei size) {
	_mutable = false;
	glNamedBufferStorage(_id, size, nullptr, _usage);
}
/*void Buffer::allocateImutable(GLsizei size, GLenum usage, GLenum target) {
	_mutable = false;
	_usage = usage;
	_target = target;
	glNamedBufferStorage(_id, size, nullptr, _usage);
}*/
void Buffer::setData(GLvoid *data, GLsizei size) {
	setSubData(data, size, 0);
}
void Buffer::setSubData(GLvoid *data, GLsizei size, GLintptr offset) {
	if (_mutable) {
		glNamedBufferSubData(_id, offset, size, data);
	}
}
void Buffer::addData(GLvoid *data, GLsizei size) {
	/*	_data = data;
	if (_mutable) {
	glNamedBufferData(_bufferID, size, _data, GL_STATIC_DRAW);
	} else {
	glNamedBufferStorage(_bufferID, size, _data, GL_STATIC_DRAW);
	}*/
}
/*void Buffer::addData(GLvoid *data, GLsizei size, GLintptr offset) {

}*/
void Buffer::getData(GLvoid *data, GLsizeiptr size) {
	getSubData(data, size, 0);
}
void Buffer::getSubData(GLvoid *data, GLsizeiptr size, GLintptr offset) {
	glGetNamedBufferSubData(_id, offset, size, data);
}






/*
void Buffer::set(GLsizei size, GLenum usage, GLvoid *data, GLsizei offset) {
	GLenum _usage = usage;
	if (_mutable) {
		glNamedBufferData(_id, size, data, _usage);
	} else {
		glNamedBufferStorage(_id, size, data, _usage);
	}
}

void Buffer::set(GLsizei size, GLvoid *data, GLsizei offset) {
	if (offset == -1) {
		if (_mutable) {
			glNamedBufferData(_id, size, data, _usage);
		} else {
			glNamedBufferStorage(_id, size, data, _usage);
		}
	} else {
		if (_mutable) {
			glNamedBufferSubData(_id, offset, size, data);
		}
//		else { printf("problem\n"); }
	}
}*/

//vytvorit
//addData glNamedBufferSubData?
//setData glNamedBufferData?
//bind

//dodelat uniformy

} //end namespace OGL

