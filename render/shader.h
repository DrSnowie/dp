//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <memory>

#include "opengl.h"
//#include <string>

namespace OGL {

class Shader {
	// nejaka abstrakce pro usnadneni prace s shadery
	// asi i nacitat shadery ze souboru spis nez to mit ve stringu

public:
	Shader(GLenum shaderType);
	Shader(GLenum shaderType, const GLchar * const shaderSource);
	Shader(GLenum shaderType, const std::string &shaderSource);
	//Shader(GLenum shaderType, const GLchar * const shaderSource);
	~Shader();
	void loadShader(const GLchar *shaderSource);
	void loadShader(const std::string &shaderSource);
	//void useShader();
//	void deleteShader();

	GLuint getShader() const;
	//???


private:
	GLuint _shader = 0;
	const GLchar *_source;

};

} // end namespace OGL

