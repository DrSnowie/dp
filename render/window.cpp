//autor: Michael Ončo
#include "window.h"

namespace OGL {

Uint32 Window::_windowsOpened = 0;

bool Window::_initialized = false;

Window::~Window() {
	if (_windowOpened) close();
	quit();
}

void Window::init(Uint32 flags /*= SDL_INIT_VIDEO*/) {
	if (!_initialized) {
		_initialized = true;
		SDL_Init(flags);
	}
}

void Window::quit() {
	if (_windowsOpened == 0 && _initialized) SDL_Quit();
}

void Window::open() {
	init();
	_windowsOpened++;
	_window = SDL_CreateWindow(_windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _windowWidth, _windowHeight, SDL_WINDOW_SHOWN);//SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
}

int Window::run() {
	bool quit = false;
	SDL_Event e;

	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			//			else if (e.type == SDL_TEXTINPUT)
			//			{
			//				int x = 0, y = 0;
			//				SDL_GetMouseState(&x, &y);
			//				handleKeys(e.text.text[0], x, y);
			//			}
		}
		//		SDL_GL_SwapWindow(_window);
	}
	return 0;
}

void Window::close() {
	SDL_DestroyWindow(_window);
	_window = NULL;

	_windowsOpened--;
}

} // end namespace OGL

