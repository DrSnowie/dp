//autor: Michael Ončo
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "camera.h"

#include <iostream>
#include <functional>

#include <orbitmanipulator.h>

namespace OGL {

Camera camera(800.0f/600.0f, 90.0f, 0.1f, 9999.0f);

OrbitManipulator::OrbitManipulator() {
	_camera = &camera;
	_projectionMatrix = _camera->getProjection();
	changeDistance(-10);
	static bool testMove = false;

	inputFunction = [&](SDL_Event &e) {
	#define UP(x) std::cout << x << " up\n"
	#define DOWN(x) std::cout << x << " down\n"
	#define MOTION(x,y) std::cout << x << " : " << y << " motion\n"
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_LEFT) {
				this->_leftMouseDown = true;
//				DOWN("LMB");
			} else if (e.button.button == SDL_BUTTON_X1 && !testMove) {
				testMove = true;
				this->rotate(10, 0);
				
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_LEFT) {
				this->_leftMouseDown = false;
//				UP("LMB");
			} else if (e.button.button == SDL_BUTTON_X1 && testMove) {
				testMove = false;
			}
			break;
		case SDL_MOUSEMOTION:
			if (this->_leftMouseDown) {
//				this->rotate(e.motion.xrel, 0);
				this->rotate(e.motion.xrel, e.motion.yrel);
//				MOTION(e.motion.xrel, e.motion.yrel);
			}
			break;
		case SDL_MOUSEWHEEL:
			this->changeDistance(e.wheel.y);
//			MOTION(e.wheel.y, "wheel");
			break;
		case SDL_KEYDOWN:
			break;
		default:
			break;
		}
	#undef UP
	#undef DOWN
			
		return 0;
	};
}

OrbitManipulator::OrbitManipulator(Camera *camera) {
	_camera = camera;
	_projectionMatrix = _camera->getProjection();
	changeDistance(-10);

	inputFunction = [&](SDL_Event &e) {
	#define UP(x) std::cout << x << " up\n"
	#define DOWN(x) std::cout << x << " down\n"
	#define MOTION(x,y) std::cout << x << " : " << y << " motion\n"
		switch (e.type) {
		case SDL_MOUSEBUTTONDOWN:
			if (e.button.button == SDL_BUTTON_LEFT) {
				this->_leftMouseDown = true;
				DOWN("LMB");
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (e.button.button == SDL_BUTTON_LEFT) {
				this->_leftMouseDown = false;
				UP("LMB");
			}
			break;
		case SDL_MOUSEMOTION:
			if (this->_leftMouseDown) {
				this->rotate(e.motion.xrel, e.motion.yrel);
				MOTION(e.motion.xrel, e.motion.yrel);
			}
			break;
		case SDL_MOUSEWHEEL:
			this->changeDistance(e.wheel.y);
			MOTION(e.wheel.y, "wheel");
			break;
		case SDL_KEYDOWN:
			break;
		default:
			break;
		}
	#undef UP
	#undef DOWN
			
		return 0;
	};
}

void OrbitManipulator::rotate(int x, int y) {
	const glm::vec3 _center = glm::vec3(0.0f, 0.0f, 0.0f);
	
	glm::vec4 position = glm::vec4(0.0f,0.0f,0.0f,1.0f);
	
	_angleX += x * _rotationSensitivity;
	_angleY += y * _rotationSensitivity;

	_angleY = glm::clamp(_angleY, -glm::half_pi<float>(), glm::half_pi<float>());

	_rotationMatrix = glm::rotate(glm::mat4(1.0f), _angleY, glm::vec3(1.0f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), _angleX, glm::vec3(0.0f, 1.0f, 0.0f));

	_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, _distance)) * _rotationMatrix;
	_matrix = _projectionMatrix * _matrix;
}

void OrbitManipulator::changeDistance(int y) {
	_distance += y * _distanceSensitivity;
//std::cout << _distance << std::endl;
	_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, _distance)) * _rotationMatrix;
	_matrix = _projectionMatrix * _matrix;

	glm::vec4 test = _matrix * glm::vec4(0,0,0,1);

//std::cout << test.x << " : " << test.y << " : " << test.z << "\n";
}

} // end namespace OGL

