//autor: Michael Ončo

#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtx/quaternion.hpp>

namespace OGL {

Camera::Camera(GLfloat aspectRatio, GLfloat verticalFOV, GLfloat near, GLfloat far) {
	this->_aspectRatio = aspectRatio;
	this->_verticalFOV = verticalFOV;
	this->_near = near;
	this->_far = far;
	this->_PMatrix = glm::perspective(glm::radians(this->_verticalFOV), this->_aspectRatio, this->_near, this->_far);
	this->_VPMatrix = this->_PMatrix * this->_VMatrix;
}

void Camera::changeCamera(GLfloat aspectRatio, GLfloat verticalFOV, GLfloat near, GLfloat far) {
	this->_aspectRatio = aspectRatio;
	this->_verticalFOV = verticalFOV;
	this->_near = near;
	this->_far = far;
	this->_PMatrix = glm::perspective(glm::radians(this->_verticalFOV), this->_aspectRatio, this->_near, this->_far);
	this->_VPMatrix = this->_PMatrix * this->_VMatrix;
}

glm::mat4 Camera::getRotationMatrix() {
//	glm::mat4 matrix
//		= glm::rotate(glm::mat4(1.0f), this->_rotationAngles.x, glm::vec3(1, 0, 0))
//		* glm::rotate(glm::mat4(1.0f), this->_rotationAngles.y, glm::vec3(0, 1, 0))
//		* glm::rotate(glm::mat4(1.0f), this->_rotationAngles.z, glm::vec3(0, 0, 1));
//	return matrix;
	return glm::mat4(1.0f);
}

glm::mat4 Camera::getView() {
	if (this->_VMatrixValid == false) {
		this->_update();
	}
	return this->_VMatrix;
}

glm::mat4 Camera::getProjection() {
	return this->_PMatrix;
}

glm::mat4 Camera::getViewProjection() {
//	if (this->_VMatrixValid == false) {
//		this->_update();
//	}
	return glm::inverse(this->_VPMatrix);
}

void Camera::move(glm::vec3 moveVector) {
	this->_VMatrixValid = false;
	_position += glm::vec4(moveVector, 0.0f);
}

void Camera::rotate(glm::quat rotate) {
	this->_VMatrixValid = false;
//	_rotationAngles += rotateAngles;
}

void Camera::simpleRotateAroundPoint(glm::vec3 point, float x, float y) {
//	this->_VMatrixValid = ;
	
//	_rotationAngles += rotateAngles;
//	translate
//	rotate
//	translate
	auto yAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	auto xAxis = glm::vec3(1.0f, 0.0f, 0.0f);
	auto matrix = glm::mat4(1.0f);
	matrix = glm::translate(matrix, point);
	matrix = glm::rotate(matrix, x, yAxis);
	matrix = glm::rotate(matrix, y, xAxis);
	matrix = glm::translate(matrix, -point);
	_VMatrix = _VMatrix * matrix;
}

void Camera::rotateAngleX(GLfloat rotateAngle) {
	this->_VMatrixValid = false;
//	_rotationAngles.x += rotateAngle;
}

void Camera::rotateAngleY(GLfloat rotateAngle) {
	this->_VMatrixValid = false;
//	_rotationAngles.y += rotateAngle;
}

void Camera::rotateAngleZ(GLfloat rotateAngle) {
	this->_VMatrixValid = false;
//	_rotationAngles.z += rotateAngle;
}

void Camera::setPosition(glm::vec3 position) {
//	this->_VMatrixValid = false;
//	_position = glm::vec4(position, 1.0f);
	_VMatrix = glm::translate(glm::mat4(1.0f), position);
}

void Camera::setRotation(glm::quat rotation) {
	this->_VMatrixValid = false;
	_rotation = rotation;
}

//TODO should set rotation accordingly - matrix is set properly - get the rotation values from 
void Camera::lookAt(glm::vec3 objectPosition) {
	this->_VMatrixValid = true;
	this->_VMatrix = glm::lookAt(glm::vec3(this->_position), objectPosition, glm::vec3(0,1,0));
	this->_VPMatrix = this->_PMatrix * this->_VMatrix;
}


glm::vec3 Camera::getPosition() {
	return glm::vec3(this->_position);
}

glm::quat Camera::getRotation() {
	return this->_rotation;
}

void Camera::_update() {
	this->_VMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(this->_position)) * getRotationMatrix();
	this->_VPMatrix = this->_PMatrix * this->_VMatrix;
	this->_VMatrixValid = true;
}

} // end namespace OGL

