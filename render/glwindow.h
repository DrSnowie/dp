//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <list>
#include <memory>
#include <iostream>
#include <functional>

#include "program.h"
//#include "shader.h"

namespace OGL {

class GLWindow {
public:
	GLWindow();
	GLWindow(int width, int height);
	virtual ~GLWindow();


	void open();
	// should be in some window manager or something so it can handle multiple windows..
	int run();
	//void update();
	void close();

	static void init(Uint32 flags = SDL_INIT_VIDEO);
	static void quit();

	void setDimensions(Uint32 width, Uint32 height);
	void setHeight(Uint32 height);
	void setWidth(Uint32 width);
	void setName(std::string name);

	Uint32 getWidth();
	Uint32 getHeight();

	void setProgram(std::shared_ptr<Program> program);

	void addInputFunction(std::function<int(SDL_Event &e)> input);
	void addUpdateFunction(std::function<int(float dt)> update);
	void addRenderFunction(std::function<int()> function);

/*	void setInputFunction(std::function<void()> input);
	void setUpdateFunction(std::function<void(float dt)> update);
	void setRenderFunction(std::function<void()> render);*/

private:
	Uint32 _windowHeight = 600;
	Uint32 _windowWidth = 800;

	//static std::list<std::shared_ptr<SDL_Window>> createdWindows;
	static bool _initialized;
	static Uint32 _windowsOpened;
	bool _windowOpened = false;

	std::string _windowName = "Window";
	SDL_Window* _window;
	SDL_GLContext _context;
	std::shared_ptr<Program> _program;

//	void _render();

	std::list<std::function<int(SDL_Event &e)>> _inputFunctions;
	std::list<std::function<int(float dt)>> _updateFunctions;
	std::list<std::function<int()>> _renderFunctions;
/*	std::function<void()> _inputFunction;
	std::function<void(float dt)> _updateFunction;
	std::function<void()> _renderFunction;*/

	//SDL_GLContext _context;

	//funkce na update
	//funkce render?
	//funkce na zpracovani vstupu
};

} // end namespace OGL

