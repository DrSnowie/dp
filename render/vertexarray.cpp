//autor: Michael Ončo
#include "vertexarray.h"

namespace OGL {

VertexArray::~VertexArray() {
	glDeleteVertexArrays(1, &_vertexArray);
}

VertexArray::VertexArray() {
	glGenVertexArrays(1, &_vertexArray);
}

void VertexArray::bind() {
	glBindVertexArray(_vertexArray);
}

void VertexArray::addAttribute(GLuint location, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *offset) {
//	std::shared_ptr<Attribute> attribute = std::make_shared<Attribute>(location, size, type, normalized, stride, offset);
//	_attributes.push_back(attribute);

//	glVertexArrayAttribBinding(_vertexArray, location, location);
//	glEnableVertexArrayAttrib(_vertexArray, location);

	glEnableVertexAttribArray(location);
	glVertexAttribPointer(location, size, type, normalized, stride, offset);
}

} //end namespace OGL