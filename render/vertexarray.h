//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <list>
#include <memory>

namespace OGL {

/*
struct Attribute {
	GLuint location;
	GLint size;
	GLenum type;
	GLboolean normalized;
	GLsizei stride;
	const GLvoid *offset;

	Attribute(GLuint location, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *offset) {
		this->location = location;
		this->size = size;
		this->type = type;
		this->normalized = normalized;
		this->stride = stride;
		this->offset = offset;
	}
};*/

class VertexArray {
public:
	~VertexArray();
	VertexArray();
	void addAttribute(GLuint location, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *offset);
//	static void addAttribute(GLuint location, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *offset);
	void bind();
	static void unbind();
private:
	static GLuint _bound;// = 0;
	GLuint _vertexArray = 0;
//	std::list<std::shared_ptr<Attribute>> _attributes;
};

} // end namespace OGL

