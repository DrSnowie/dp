//autor: Michael Ončo
#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "shader.h"
#include "opengl.h"

namespace OGL {

class Program {
public:
	Program();
	~Program();

	void attachShader(std::shared_ptr<Shader> shader);
	void attachShader(Shader *shader);
	void detachShader(Shader *shader);
	void useProgram();
	void link();

	void uniformMat4(std::string name, const glm::mat4 &matrix);
	void uniformVec4(std::string name, const glm::vec4 &vector);
	void uniformFloat(std::string name, const GLfloat number);
	void uniformUint(std::string name, const GLuint number);
	
	GLint getAttributeLocation(const GLchar *name);
	/*GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	// Send our transformation to the currently bound shader, in the "MVP" uniform
	// This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);*/

private:
	GLuint _program = 0;
	bool _linked = false;
	// nejaka abstrakce pro usnadneni prace s programem
	// + nejake spojeni dohromady s shadery a buffery
	// pokud to neni rovnou v ramci programu uz trochu resene

	//vsude to je ale nevim moc co to ma vlastne byt a co to ma presne delat...



	//dodelat uniformy
};


/*
GLint Program::getUniformLocation(std::string const&name)const{
  assert(this!=nullptr);
  return this->_gl.glGetUniformLocation(this->_id,name.c_str());
  }
*/

} // end namespace OGL

