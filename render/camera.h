//autor: Michael Ončo
#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

namespace OGL {

class Camera {
	
public:
	Camera(GLfloat aspectRatio, GLfloat verticalFOV, GLfloat near, GLfloat far);
	void changeCamera(GLfloat aspectRatio, GLfloat verticalFOV, GLfloat near, GLfloat far);
	void move(glm::vec3 moveVector);
	void simpleRotateAroundPoint(glm::vec3 point, float x, float y);
	void rotate(glm::quat rotateVector);
	void rotateAngleX(GLfloat rotateAngle);
	void rotateAngleY(GLfloat rotateAngle);
	void rotateAngleZ(GLfloat rotateAngle);
	void lookAt(glm::vec3 objectPosition);
	void setPosition(glm::vec3 position);
	void setRotation(glm::quat rotation);
	
	glm::vec3 getPosition();
	glm::quat getRotation();
	glm::mat4 getRotationMatrix();
	glm::mat4 getView();
	glm::mat4 getProjection();
	glm::mat4 getViewProjection();
	
private:
	
	void _update();
	
	bool _VMatrixValid = false;
	
	GLfloat _aspectRatio;
	GLfloat _near;
	GLfloat _far;
	GLfloat _verticalFOV;
	
	glm::vec4 _position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	//glm::vec3 _rotationAngles = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::quat _rotation = glm::quat(0.0f, 0.0f, 0.0f, 0.0f);
	
	glm::mat4 _PMatrix;
	glm::mat4 _VMatrix;
	glm::mat4 _VPMatrix;
};

/*
class Manipulator {
public:
	void handleInput();
	const Camera *getCamera();
private:
	Camera _camera;

};
*/

} // end namespace OGL

