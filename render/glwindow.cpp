//autor: Michael Ončo
#include "glwindow.h"

namespace OGL {

Uint32 GLWindow::_windowsOpened = 0;

bool GLWindow::_initialized = false;

GLWindow::GLWindow() {

	//init();
	//_initialized = true;
	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);


	_windowsOpened++;
	_window = SDL_CreateWindow(_windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _windowWidth, _windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (_window == NULL) {
		std::cerr << "window error\n";
	}
	_context = SDL_GL_CreateContext(_window);
	if (_context == NULL) {
		std::cerr << "context error\n";
	}
//	SDL_GL_MakeCurrent(_window, _context);

	glewExperimental = GL_TRUE;
	glewInit();
	SDL_GL_SetSwapInterval(1);
}


GLWindow::GLWindow(int width, int height) {

	//init();
	//_initialized = true;
	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	_windowWidth = width;
	_windowHeight = height;
	_windowsOpened++;
	_window = SDL_CreateWindow(_windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _windowWidth, _windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (_window == NULL) {
		std::cerr << "window error\n";
	}
	_context = SDL_GL_CreateContext(_window);
	if (_context == NULL) {
		std::cerr << "context error\n";
	}
//	SDL_GL_MakeCurrent(_window, _context);

	glewExperimental = GL_TRUE;
	glewInit();
	SDL_GL_SetSwapInterval(1);
}

GLWindow::~GLWindow() {

	SDL_DestroyWindow(_window);
	_window = NULL;

	_windowsOpened--;

	quit();
}

void GLWindow::init(Uint32 flags /*= SDL_INIT_VIDEO*/) {
	if (!_initialized) {
		_initialized = true;
		SDL_Init(flags);

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
//		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
//		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
//		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	}
}

void GLWindow::quit() {
	//if (_windowsOpened == 0 && _initialized) SDL_Quit();
	SDL_Quit();
}

void GLWindow::open() {
}


int GLWindow::run() {
	bool quit = false;
	SDL_Event e;
	float dt = 0.01f;
	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			for (auto function : _inputFunctions) {
				if (function(e) == 1) quit = true;
			}

			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
		}

		for (auto function : _updateFunctions) {
			if (function(dt) == 1) quit = true;
		}

		for (auto function : _renderFunctions) {
			function();
		}

		SDL_GL_SwapWindow(_window);
	}
	return 0;
}

void GLWindow::addInputFunction(std::function<int(SDL_Event &e)> function) {
	this->_inputFunctions.push_back(function);
};

void GLWindow::addUpdateFunction(std::function<int(float dt)> function) {
	this->_updateFunctions.push_back(function);
};

void GLWindow::addRenderFunction(std::function<int()> function) {
	this->_renderFunctions.push_back(function);
};

void GLWindow::close() {
}

void GLWindow::setProgram(std::shared_ptr<Program> program) {
	_program = program;
}

Uint32 GLWindow::getWidth() {
	return this->_windowWidth;
}

Uint32 GLWindow::getHeight() {
	return this->_windowHeight;
}

} // end namespace OGL

