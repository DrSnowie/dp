//autor: Michael Ončo
#include "shader.h"

namespace OGL {

Shader::Shader(GLenum shaderType) {
	_shader = glCreateShader(shaderType);
}

Shader::Shader(GLenum shaderType, const GLchar * const shaderSource) {
	_shader = glCreateShader(shaderType);
//	_source = shaderSource;

	glShaderSource(_shader, 1, &shaderSource, NULL);
	glCompileShader(_shader);
//	glShaderSource(vertexShader, 1, vertexShaderSource, NULL);

	GLint compilation = GL_FALSE;
	glGetShaderiv(_shader, GL_COMPILE_STATUS, &compilation);
	if (compilation != GL_TRUE)
	{
		std::cerr << "Unable to compile shader " << _shader << "!\n";
		printShaderLog(_shader);
	}
}

Shader::Shader(GLenum shaderType, const std::string &shaderSource) {
	_shader = glCreateShader(shaderType);
//	_source = shaderSource;
	const GLchar * const source = shaderSource.c_str();
	glShaderSource(_shader, 1, &source, NULL);
	glCompileShader(_shader);
//	glShaderSource(vertexShader, 1, vertexShaderSource, NULL);

	GLint compilation = GL_FALSE;
	glGetShaderiv(_shader, GL_COMPILE_STATUS, &compilation);
	if (compilation != GL_TRUE)
	{
		std::cerr << "Unable to compile shader " << _shader << "!\n";
		printShaderLog(_shader);
	}
}

Shader::~Shader() {
	glDeleteShader(_shader);
}

void Shader::loadShader(const GLchar *shaderSource) {
//	glShaderSource(vertexShader, 1, vertexShaderSource, NULL);
}
//void useShader();
//	void deleteShader();

GLuint Shader::getShader() const {
	return _shader;
}
	//???


} // end namespace OGL

