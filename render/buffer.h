//autor: Michael Ončo
#pragma once
#include <GL/glew.h>

namespace OGL {

class Buffer {
public:
	~Buffer();

	Buffer(GLenum usage);
//	Buffer(GLenum usage, GLsizei size);
//	Buffer(GLenum usage, GLsizei size, GLvoid *data);
//	Buffer(GLenum usage, GLsizei size, GLvoid *data, GLsizei dataSize);
	Buffer(GLenum usage, GLenum target, GLuint position, GLsizei size, GLvoid *data, GLsizei dataSize);
	Buffer(GLenum usage, GLenum target, GLuint position, GLsizei size, GLvoid *data);
	Buffer(GLenum usage, GLenum target, GLsizei size, GLvoid *data, GLsizei dataSize);
	Buffer(GLenum usage, GLenum target, GLsizei size, GLvoid *data);
	//Buffer(GLenum target, GLsizei size = 0, GLvoid *data = nullptr);
//	void set(GLsizei size, GLenum usage, GLvoid *data = nullptr, GLsizei offset = -1);
//	void set(GLsizei size, GLvoid *data = nullptr, GLsizei offset = -1);

	void bind(GLenum target, GLuint position);
	void bind(GLenum target);
//	void bind(GLuint position);
//	void bind();

	//TODO this seems to be a good subset of methods - figure out how to do binds nicely and targets
	void getData(GLvoid *data, GLsizeiptr size);
	void getSubData(GLvoid *data, GLsizeiptr size, GLintptr offset);
	void addData(GLvoid *data, GLsizei size);
//	void addData(GLvoid *data, GLsizei size, GLintptr offset);
	void setData(GLvoid *data, GLsizei size);
	void setSubData(GLvoid *data, GLsizei size, GLintptr offset);
//	void setTarget(GLenum target);


	void createMutable(GLenum usage, GLsizei size, GLvoid *data, GLsizei dataSize);
	void createMutable(GLsizei size, GLvoid *data, GLsizei dataSize);
	void createMutable(GLenum usage, GLsizei size, GLvoid *data);
	void createMutable(GLsizei size, GLvoid *data);
	void allocateMutable(GLsizei size);
	void allocateMutable(GLenum usage, GLsizei size);
//	void allocateMutable(GLsizei size, GLenum usage, GLenum target);
	void createImutable(GLenum usage, GLsizei size, GLvoid *data);
	void createImutable(GLsizei size, GLvoid *data);
	void allocateImutable(GLenum usage, GLsizei size);
	void allocateImutable(GLsizei size);
//	void allocateImutable(GLsizei size, GLenum usage, GLenum target);
	//void resize();

	static void unbind(GLenum target) {
		glBindBuffer(target, 0);
	}

private:

//	void *_data;
	GLsizei _bufferSize;

	GLuint _position;
	GLuint _id = 0;
//	GLuint _target = GL_SHADER_STORAGE_BUFFER;
	GLuint _usage = GL_STATIC_READ;
	
	GLboolean _mutable = true;

	//vytvorit
	//addData glNamedBufferSubData?
	//setData glNamedBufferData?
	//bind
	//_velikost
	//_index konce posledniho pridani

	//dodelat vertexattributy
	//dodelat uniformy
};


//TODO buffer mapping - gets direct pointer to write to

} // end namespace OGL

